# RapidCheck User Guide

The following document is a work in progress and some parts are still missing. There is more documentation to read in the form of source code comments if something is missing here.

**Also, please note that the API has not stabilized yet and may change at any time.**

## Basics

- [Properties](calendar/tests/deps/rapidcheck/doc/properties.md)
- [Generators](calendar/tests/deps/rapidcheck/doc/generators.md)
- [Displaying values](calendar/tests/deps/rapidcheck/doc/displaying.md)
- [Assertions](calendar/tests/deps/rapidcheck/doc/assertions.md)
- [Reporting distribution](calendar/tests/deps/rapidcheck/doc/distribution.md)
- [Configuration](calendar/tests/deps/rapidcheck/doc/configuration.md)
- [Debugging failures](calendar/tests/deps/rapidcheck/doc/debugging.md)
- [Stateful testing](calendar/tests/deps/rapidcheck/doc/state.md)

## Extras

- [Google Test integration](calendar/tests/deps/rapidcheck/doc/gtest.md)
- [Google Mock integration](calendar/tests/deps/rapidcheck/doc/gmock.md)
- [Boost support](calendar/tests/deps/rapidcheck/doc/boost.md)
- [Boost Test integration](calendar/tests/deps/rapidcheck/doc/boost_test.md)

## Reference

- [Generators](calendar/tests/deps/rapidcheck/doc/generators_ref.md)
- [Stateful testing](calendar/tests/deps/rapidcheck/doc/state_ref.md)

## Advanced topics

- [Gen\<T\>](calendar/tests/deps/rapidcheck/doc/Gen.md)
- [Shrinkable\<T\>](calendar/tests/deps/rapidcheck/doc/Shrinkable.md)
- [Seq\<T\>](calendar/tests/deps/rapidcheck/doc/Seq.md)
