#include "common.h"
#include "src/calendars/julian_day.h"

TEST_SUITE("Julian Day") {
  TEST_CASE("Conversions") {
    rc::check([](int32_t day) { REQUIRE_EQ(calendars::Date::from(calendars::Date{day}.to<calendars::JulianDay>()).day, day); });
  }
}


