#include "common.h"
#include "src/calendars/egyptian_date.h"

TEST_SUITE("Egyptian Date") {
  TEST_CASE("Conversions") {
    rc::check([](int32_t day) { REQUIRE_EQ(calendars::Date::from(calendars::Date{day}.to<calendars::EgyptianDate>()).day, day); });
  }
}

