#include "common.h"
#include "src/calendars/julian_date.h"

TEST_SUITE("Julian Date") {
  TEST_CASE("Conversions") {
    rc::check([](int32_t day) { REQUIRE_EQ(calendars::Date::from(calendars::Date{day}.to<calendars::JulianDate>()).day, day); });
  }

  TEST_CASE("Comparisons") {
    auto res = rc::check([](int32_t leftYear, int8_t leftMonth, int16_t leftDay, int32_t rightYear, int8_t rightMonth, int16_t rightDay) {
      auto leftDate  = calendars::JulianDate{leftYear, leftMonth, leftDay};
      auto rightDate = calendars::JulianDate{rightYear, rightMonth, rightDay};

      bool okay = true;
      if (leftDate.year == rightDate.year && leftDate.month == rightDate.month && leftDate.day == rightDate.day) {
        ASSERT(leftDate <= rightDate)
        ASSERT(leftDate == rightDate)
        ASSERT(leftDate >= rightDate)
        ASSERT_NOT(leftDate < rightDate)
        ASSERT_NOT(leftDate > rightDate)
      }
      else {
        ASSERT(leftDate != rightDate)
      }
      return okay;
    });
    CHECK(res);
  }
}


