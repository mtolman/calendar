#include "common.h"
#include "src/calendars/gregorian_date.h"

TEST_SUITE("Gregorian Date") {
  TEST_CASE("Conversions") {
    rc::check([](int32_t day) { REQUIRE_EQ(calendars::Date::from(calendars::Date{day}.to<calendars::GregorianDate>()).day, day); });
  }

  TEST_CASE("Comparisons") {
    auto res = rc::check([](int32_t leftYear, int8_t leftMonth, int16_t leftDay, int32_t rightYear, int8_t rightMonth, int16_t rightDay) {
      leftMonth  = calendars::math::amod(leftMonth, 12);
      rightMonth = calendars::math::amod(rightMonth, 12);

      leftDay  = calendars::math::amod(leftDay, 31);
      rightDay = calendars::math::amod(rightDay, 31);

      auto leftDate  = calendars::GregorianDate{leftYear, leftMonth, leftDay};
      auto rightDate = calendars::GregorianDate{rightYear, rightMonth, rightDay};

      bool okay = true;
      if (leftDate.year == rightDate.year && leftDate.month == rightDate.month && leftDate.day == rightDate.day) {
        ASSERT(leftDate <= rightDate)
        ASSERT(leftDate == rightDate)
        ASSERT(leftDate >= rightDate)
        ASSERT_NOT(leftDate < rightDate)
        ASSERT_NOT(leftDate > rightDate)
      }
      else {
        ASSERT(leftDate != rightDate)
      }
      return okay;
    });
    CHECK(res);
  }

  TEST_CASE("Ranges") {
      using G = calendars::GregorianDate;
      SUBCASE("Year Start") {
        REQUIRE(rc::check([](int32_t year) { return CHECK_RET((G{year, G::MONTH::JANUARY, 1} == G::year_start(year).to<G>()), year); }));
      }

      SUBCASE("Year End") {
        REQUIRE(rc::check([](int32_t year) { return CHECK_RET((G{year, G::MONTH::DECEMBER, 31} == G::year_end(year).to<G>()), year); }));
      }

      SUBCASE("Year Boundaries") {
        rc::check([](int32_t year) {
          REQUIRE_EQ(G{2020, G::MONTH::JANUARY, 1}, std::get<0>(G::year_boundaries(2020)).to<G>());
          REQUIRE_EQ(G{2020, G::MONTH::DECEMBER, 31}, std::get<1>(G::year_boundaries(2020)).to<G>());
        });
      }

      SUBCASE("Year Vector") {
        rc::check([](int32_t year) {
          const auto expectedDays = G::is_leap_year(year) ? 366 : 365;
          auto       v            = G::year_vector(year);
          REQUIRE_EQ(expectedDays, v.size());
          REQUIRE_EQ(G{year, G::MONTH::JANUARY, 1}, v[ 0 ].to<G>());
          REQUIRE_EQ(G{year, G::MONTH::DECEMBER, 31}, v[ v.size() - 1 ].to<G>());

          for (size_t i = 1; i < v.size(); ++i) {
            REQUIRE_EQ(v[ i ].day_difference(v[ i - 1 ]), 1);
          }
        });
      }

      SUBCASE("Year Range Constexpr") {
        SUBCASE("Non-leap") {
          constexpr auto year = 2019;
          const auto     v    = G::year_range_constexpr<year>();
          REQUIRE_EQ(v.size(), 365);
          CHECK_EQ(G{year, G::MONTH::JANUARY, 1}, v[ 0 ].to<G>());
          CHECK_EQ(G{year, G::MONTH::DECEMBER, 31}, v[ v.size() - 1 ].to<G>());

          for (size_t i = 1; i < v.size(); ++i) {
            CHECK_EQ(v.at(i).day_difference(v.at(i - 1)), 1);
          }
        }

        SUBCASE("Leap") {
          constexpr auto year = 2020;
          const auto     v    = G::year_range_constexpr<year>();
          REQUIRE_EQ(v.size(), 366);
          CHECK_EQ(G{year, G::MONTH::JANUARY, 1}, v[ 0 ].to<G>());
          CHECK_EQ(G{year, G::MONTH::DECEMBER, 31}, v[ v.size() - 1 ].to<G>());

          for (size_t i = 1; i < v.size(); ++i) {
            CHECK_EQ(v.at(i).day_difference(v.at(i - 1)), 1);
          }
        }
      }
    }
}


