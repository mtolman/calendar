#include "common.h"
#include "src/calendars/old_hindu_solar_date.h"

TEST_SUITE("Old Hindu Solar Date Date") {
  TEST_CASE("Conversions") {
    rc::check([](int32_t day) { REQUIRE_EQ(calendars::Date::from(calendars::Date{day}.to<calendars::OldHinduSolarDate>()).day, day); });
  }
}


