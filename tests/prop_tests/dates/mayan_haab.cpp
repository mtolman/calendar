#include "common.h"
#include "src/calendars/mayan/haab.h"

TEST_SUITE("Mayan Haab") {
  TEST_CASE("haab on or before") {
    auto res = rc::check([](int n, int32_t day) {
      auto date    = calendars::Date{day};
      auto newDate = calendars::mayan::haab_on_or_before(date,
                                                         calendars::mayan::Haab{
                                                             calendars::mayan::Haab::MONTH::KANKIN,
                                                             calendars::math::mod(n, 20)
                                                         }
      );

      return CHECK_RET(newDate <= date && newDate >= date.sub_days(365), day);
    });
    CHECK(res);
  }
}

