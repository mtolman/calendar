#pragma once

#include "../common.hpp"
#include <doctest.h>
#include <common_test_headers.h>
#include <rapidcheck.h>

// NOLINTNEXTLINE(cppcoreguidelines-macro-usage)
#define ASSERT(E)                                                                                                                                    \
  CHECK(E);                                                                                                                                          \
  RC_ASSERT(E);                                                                                                                                      \
  if (!(E)) okay = false;

// NOLINTNEXTLINE(cppcoreguidelines-macro-usage)
#define ASSERT_NOT(E)                                                                                                                                \
  CHECK_FALSE(E);                                                                                                                                    \
  RC_ASSERT(!(E));                                                                                                                                   \
  if ((E)) okay = false;
