#include "common.h"
#include "src/calendars/hebrew_date.h"

TEST_SUITE("Hebrew Date") {
  TEST_CASE("Conversions") {
    rc::check([](int32_t day) { REQUIRE_EQ(calendars::Date::from(calendars::Date{day}.to<calendars::HebrewDate>()).day, day); });
  }
}

