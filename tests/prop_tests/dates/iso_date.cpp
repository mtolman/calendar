#include "common.h"
#include "src/calendars/iso_date.h"

TEST_SUITE("ISO Date") {
  TEST_CASE("Conversions") {
    rc::check([](int32_t day) { REQUIRE_EQ(calendars::Date::from(calendars::Date{day}.to<calendars::IsoDate>()).day, day); });
  }
}


