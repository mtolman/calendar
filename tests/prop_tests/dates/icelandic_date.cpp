#include "common.h"
#include "src/calendars/icelandic_date.h"

TEST_SUITE("Icelandic Date") {
  TEST_CASE("Conversions") {
    rc::check([](int32_t day) { REQUIRE_EQ(calendars::Date::from(calendars::Date{day}.to<calendars::IcelandicDate>()).day, day); });
  }
}


