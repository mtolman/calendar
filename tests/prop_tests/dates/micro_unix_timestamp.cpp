#include "common.h"
#include "src/calendars/micro_unix_timestamp.h"

TEST_SUITE("Micro Unix Timestamp") {
  TEST_CASE("Conversions") {
    rc::check([](int32_t day) { REQUIRE_EQ(calendars::Date::from(calendars::Date{day}.to<calendars::MicroUnixTimestamp>()).day, day); });
  }

  TEST_CASE("Comparisons") {
      auto res = rc::check([](int32_t left, int32_t right) {
        auto leftDate  = calendars::MicroUnixTimestamp{static_cast<double>(left)};
        auto rightDate = calendars::MicroUnixTimestamp{static_cast<double>(right)};

        bool okay = true;
        if (left == right) {
          ASSERT(leftDate <= rightDate)
          ASSERT(leftDate == rightDate)
          ASSERT(leftDate >= rightDate)
          ASSERT_NOT(leftDate < rightDate)
          ASSERT_NOT(leftDate > rightDate)
        }
        else if (left < right) {
          ASSERT(leftDate < rightDate)
          ASSERT(leftDate <= rightDate)
          ASSERT_NOT(leftDate == rightDate)
          ASSERT_NOT(leftDate >= rightDate)
          ASSERT_NOT(leftDate > rightDate)
        }
        else {
          ASSERT_NOT(leftDate <= rightDate)
          ASSERT_NOT(leftDate == rightDate)
          ASSERT_NOT(leftDate < rightDate)
          ASSERT(leftDate >= rightDate)
          ASSERT(leftDate > rightDate)
        }
        return okay;
      });
      CHECK(res);
    }
}


