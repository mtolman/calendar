#include "common.h"
#include "src/calendars/old_hindu_luni_solar_date.h"

TEST_SUITE("Old Hindu Luni Solar Date") {
  TEST_CASE("Conversions") {
    rc::check([](int32_t day) { REQUIRE_EQ(calendars::Date::from(calendars::Date{day}.to<calendars::OldHinduLuniSolarDate>()).day, day); });
  }
}


