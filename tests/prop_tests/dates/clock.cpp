#include "common.h"
#include "src/calendars/clock.h"

TEST_SUITE("Clock") {
  TEST_CASE("Clock nearest_second") {
    rc::check([](double d) {
      auto clock = calendars::Moment{d}.to<calendars::Clock>().nearest_second();
      CHECK_EQ(clock.date.value[ calendars::Clock::DAY ], 0);
      CHECK((clock.date.value[ calendars::Clock::HOUR ] <= 24 && clock.date.value[ calendars::Clock::HOUR ] >= 0));
      CHECK((clock.date.value[ calendars::Clock::MINUTE ] <= 60 && clock.date.value[ calendars::Clock::HOUR ] >= 0));
      CHECK((clock.date.value[ calendars::Clock::SECOND ] <= 60 && clock.date.value[ calendars::Clock::HOUR ] >= 0));
    });
  }

  TEST_CASE("Clock time") {
    rc::check([](double d) {
      auto time = calendars::Moment{d}.to<calendars::Clock>().time();
      CHECK((time < 1.0 && time >= 0));
    });
  }
}
