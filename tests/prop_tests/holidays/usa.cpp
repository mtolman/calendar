#include "common.h"

TEST_SUITE("USA Holidays") {
  TEST_CASE("Independence Day") {
    rc::check([](int32_t year) {
      auto d = c::Date{c::GregorianDate{year, c::GregorianDate::MONTH::JULY, 4}};
      REQUIRE_EQ(d, h::usa::independence_day(year));
    });
  }

  TEST_CASE("Labor Day") {
    rc::check([](int32_t year) {
      auto d = c::Date{c::GregorianDate{year, c::GregorianDate::MONTH::SEPTEMBER, 1}};
      auto a = h::usa::labor_day(year);
      REQUIRE_LE(d, a);
      REQUIRE_GE(d.add_days(7), a);
      REQUIRE_EQ(a.day_of_week(), c::DAY_OF_WEEK::MONDAY);
    });
  }

  TEST_CASE("Memorial Day") {
    rc::check([](int32_t year) {
      auto d = c::Date{c::GregorianDate{year, c::GregorianDate::MONTH::MAY, 31}};
      auto a = h::usa::memorial_day(year);
      REQUIRE_GE(d, a);
      REQUIRE_LE(d.sub_days(7), a);
      REQUIRE_EQ(a.day_of_week(), c::DAY_OF_WEEK::MONDAY);
    });
  }

  TEST_CASE("Election Day") {
    rc::check([](int32_t year) {
      auto d = c::Date{c::GregorianDate{year, c::GregorianDate::MONTH::NOVEMBER, 2}};
      auto a = h::usa::election_day(year);
      REQUIRE_LE(d, a);
      REQUIRE_GE(d.add_days(7), a);
      REQUIRE_EQ(a.day_of_week(), c::DAY_OF_WEEK::TUESDAY);
    });
  }
}
