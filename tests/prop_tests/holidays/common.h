#pragma once

#include <doctest.h>
#include <rapidcheck.h>

#include "src/calendars.h"
#include "src/holidays.h"

namespace h = calendars::holidays;
namespace c = calendars;
