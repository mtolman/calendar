#include "common.h"

TEST_SUITE("Copts Holidays") {
  TEST_CASE("Christmas") {
    SUBCASE("Coptic") {
      rc::check([](int32_t year) {
        auto holiday = h::copts::christmas(year);
        REQUIRE_EQ(holiday.to<c::CopticDate>(), c::CopticDate{year, c::CopticDate::MONTH::KOIAK, 29});
      });
    }

    SUBCASE("Gregorian") {
      rc::check([](int32_t year) {
        auto holiday = h::copts::gregorian::christmas(year);

        for (decltype(auto) date : holiday) {
          auto copticDate = date.to<c::CopticDate>();
          REQUIRE_EQ(copticDate.month, c::CopticDate::MONTH::KOIAK);
          REQUIRE_EQ(copticDate.day, 29);
          REQUIRE_EQ(date.to<c::GregorianDate>().year, year);
        }
      });
    }
  }

  TEST_CASE("Building of the Cross") {
    SUBCASE("Coptic") {
      rc::check([](int32_t year) {
        auto holiday = h::copts::building_of_the_cross(year);
        REQUIRE_EQ(holiday.to<c::CopticDate>(), c::CopticDate{year, c::CopticDate::MONTH::THOOUT, 17});
      });
    }

    SUBCASE("Gregorian") {
      rc::check([](int32_t year) {
        auto holiday = h::copts::gregorian::building_of_the_cross(year);

        for (decltype(auto) date : holiday) {
          auto copticDate = date.to<c::CopticDate>();
          REQUIRE_EQ(copticDate.month, c::CopticDate::MONTH::THOOUT);
          REQUIRE_EQ(copticDate.day, 17);
          REQUIRE_EQ(date.to<c::GregorianDate>().year, year);
        }
      });
    }
  }

  TEST_CASE("Jesus's Circumcision") {
    SUBCASE("Coptic") {
      rc::check([](int32_t year) {
        auto holiday = h::copts::jesus_circumcision(year);
        REQUIRE_EQ(holiday.to<c::CopticDate>(), c::CopticDate{year, c::CopticDate::MONTH::TOBE, 6});
      });
    }

    SUBCASE("Gregorian") {
      rc::check([](int32_t year) {
        auto holiday = h::copts::gregorian::jesus_circumcision(year);

        for (decltype(auto) date : holiday) {
          auto copticDate = date.to<c::CopticDate>();
          REQUIRE_EQ(copticDate.month, c::CopticDate::MONTH::TOBE);
          REQUIRE_EQ(copticDate.day, 6);
          REQUIRE_EQ(date.to<c::GregorianDate>().year, year);
        }
      });
    }
  }

  TEST_CASE("Epiphany") {
    SUBCASE("Coptic") {
      rc::check([](int32_t year) {
        auto holiday = h::copts::epiphany(year);
        REQUIRE_EQ(holiday.to<c::CopticDate>(), c::CopticDate{year, c::CopticDate::MONTH::TOBE, 11});
      });
    }

    SUBCASE("Gregorian") {
      rc::check([](int32_t year) {
        auto holiday = h::copts::gregorian::epiphany(year);

        for (decltype(auto) date : holiday) {
          auto copticDate = date.to<c::CopticDate>();
          REQUIRE_EQ(copticDate.month, c::CopticDate::MONTH::TOBE);
          REQUIRE_EQ(copticDate.day, 11);
          REQUIRE_EQ(date.to<c::GregorianDate>().year, year);
        }
      });
    }
  }

  TEST_CASE("Mary's Announcement") {
    SUBCASE("Coptic") {
      rc::check([](int32_t year) {
        auto holiday = h::copts::marys_announcement(year);
        REQUIRE_EQ(holiday.to<c::CopticDate>(), c::CopticDate{year, c::CopticDate::MONTH::PAREMOTEP, 29});
      });
    }

    SUBCASE("Gregorian") {
      rc::check([](int32_t year) {
        auto holiday = h::copts::gregorian::marys_announcement(year);

        for (decltype(auto) date : holiday) {
          auto copticDate = date.to<c::CopticDate>();
          REQUIRE_EQ(copticDate.month, c::CopticDate::MONTH::PAREMOTEP);
          REQUIRE_EQ(copticDate.day, 29);
          REQUIRE_EQ(date.to<c::GregorianDate>().year, year);
        }
      });
    }
  }

  TEST_CASE("Jesus's Transfiguration") {
    SUBCASE("Coptic") {
      rc::check([](int32_t year) {
        auto holiday = h::copts::jesus_transfiguration(year);
        REQUIRE_EQ(holiday.to<c::CopticDate>(), c::CopticDate{year, c::CopticDate::MONTH::MESORE, 13});
      });
    }

    SUBCASE("Gregorian") {
      rc::check([](int32_t year) {
        auto holiday = h::copts::gregorian::jesus_transfiguration(year);

        for (decltype(auto) date : holiday) {
          auto copticDate = date.to<c::CopticDate>();
          REQUIRE_EQ(copticDate.month, c::CopticDate::MONTH::MESORE);
          REQUIRE_EQ(copticDate.day, 13);
          REQUIRE_EQ(date.to<c::GregorianDate>().year, year);
        }
      });
    }
  }
}
