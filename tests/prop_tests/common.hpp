#pragma once

#include <rapidcheck.h>
#include <array>

#include "src/calendars.h"

enum class ConversionTarget
{
  RD_DATE,
  MOMENT,
  MJD_DATE,
  JD_DATE,
  UNIX,
  UNIX_MICRODATE,
  EGYPTIAN,
  ARMENIAN,
  ZOROASTRIAN,
  GREGORIAN,
  JULIAN,
  COPTIC,
  ETHIOPIC,
  ISO_DATE,
  ICELANDIC,
  ISLAMIC,
  HEBREW,
  OLD_HINDU_SOLAR,
  OLD_HINDU_LUNI_SOLAR,
  MAYAN_LONG_DATE
};

constexpr std::string_view target_name(ConversionTarget t) {
#define CASE(NAME) case ConversionTarget::NAME: return #NAME;
  switch (t) {
    CASE(RD_DATE)
    CASE(MOMENT)
    CASE(MJD_DATE)
    CASE(JD_DATE)
    CASE(UNIX)
    CASE(UNIX_MICRODATE)
    CASE(EGYPTIAN)
    CASE(ARMENIAN)
    CASE(ZOROASTRIAN)
    CASE(GREGORIAN)
    CASE(JULIAN)
    CASE(COPTIC)
    CASE(ETHIOPIC)
    CASE(ISO_DATE)
    CASE(ICELANDIC)
    CASE(ISLAMIC)
    CASE(HEBREW)
    CASE(OLD_HINDU_SOLAR)
    CASE(OLD_HINDU_LUNI_SOLAR)
    CASE(MAYAN_LONG_DATE)
  }
#undef CASE
}

namespace rc {
  template<>
  struct Arbitrary<ConversionTarget> {
    static Gen<ConversionTarget> arbitrary() {
      return gen::element(ConversionTarget::RD_DATE,
                          ConversionTarget::MOMENT,
                          ConversionTarget::MJD_DATE,
                          ConversionTarget::JD_DATE,
                          ConversionTarget::UNIX,
                          ConversionTarget::UNIX_MICRODATE,
                          ConversionTarget::EGYPTIAN,
                          ConversionTarget::ARMENIAN,
                          ConversionTarget::ZOROASTRIAN,
                          ConversionTarget::GREGORIAN,
                          ConversionTarget::JULIAN,
                          ConversionTarget::COPTIC,
                          ConversionTarget::ETHIOPIC,
                          ConversionTarget::ISO_DATE,
                          ConversionTarget::ICELANDIC,
                          ConversionTarget::ISLAMIC,
                          ConversionTarget::HEBREW,
                          ConversionTarget::OLD_HINDU_SOLAR,
                          ConversionTarget::OLD_HINDU_LUNI_SOLAR,
                          ConversionTarget::MAYAN_LONG_DATE);
    }
  };
};  // namespace rc

namespace rc {
  template<>
  struct Arbitrary<calendars::DAY_OF_WEEK> {
    static Gen<calendars::DAY_OF_WEEK> arbitrary() {
      return gen::element(calendars::DAY_OF_WEEK::SUNDAY,
                          calendars::DAY_OF_WEEK::MONDAY,
                          calendars::DAY_OF_WEEK::TUESDAY,
                          calendars::DAY_OF_WEEK::WEDNESDAY,
                          calendars::DAY_OF_WEEK::THURSDAY,
                          calendars::DAY_OF_WEEK::FRIDAY,
                          calendars::DAY_OF_WEEK::SATURDAY,
                          calendars::DAY_OF_WEEK::SUNDAY);
    }
  };
};  // namespace rc

inline decltype(auto) to_gregorian_normalized(int64_t year, int16_t month, int16_t day) {
  month = calendars::math::amod(month, 12);
  if (month == 2) {
    day = calendars::math::amod(day, calendars::GregorianDate::is_leap_year(year) ? 29 : 28);
  }
  else if (month == 1 || month == 3 || month == 5 || month == 7 || month == 8 || month == 10 || month == 12) {
    day = calendars::math::amod(day, 31);
  }
  else {
    day = calendars::math::amod(day, 30);
  }
  return calendars::GregorianDate{year, month, day};
}

inline calendars::Date convert_date(const calendars::Date& date, ConversionTarget type) {
  switch (type) {
    case ConversionTarget::RD_DATE:
      return calendars::Date::from(date.to<calendars::Date>());
    case ConversionTarget::MOMENT:
      return calendars::Date::from(date.to<calendars::Moment>());
    case ConversionTarget::MJD_DATE:
      return calendars::Date::from(date.to<calendars::ModifiedJulianDay>());
    case ConversionTarget::JD_DATE:
      return calendars::Date::from(date.to<calendars::JulianDay>());
    case ConversionTarget::UNIX:
      return calendars::Date::from(date.to<calendars::UnixTimestamp>());
    case ConversionTarget::UNIX_MICRODATE:
      return calendars::Date::from(date.to<calendars::MicroUnixTimestamp>());
    case ConversionTarget::EGYPTIAN:
      return calendars::Date::from(date.to<calendars::EgyptianDate>());
    case ConversionTarget::ARMENIAN:
      return calendars::Date::from(date.to<calendars::ArmenianDate>());
    case ConversionTarget::ZOROASTRIAN:
      return calendars::Date::from(date.to<calendars::ZoroastrianDate>());
    case ConversionTarget::GREGORIAN:
      return calendars::Date::from(date.to<calendars::GregorianDate>());
    case ConversionTarget::JULIAN:
      return calendars::Date::from(date.to<calendars::JulianDate>());
    case ConversionTarget::COPTIC:
      return calendars::Date::from(date.to<calendars::CopticDate>());
    case ConversionTarget::ETHIOPIC:
      return calendars::Date::from(date.to<calendars::EthiopianDate>());
    case ConversionTarget::ISO_DATE:
      return calendars::Date::from(date.to<calendars::IsoDate>());
    case ConversionTarget::ICELANDIC:
      return calendars::Date::from(date.to<calendars::IcelandicDate>());
    case ConversionTarget::ISLAMIC:
      return calendars::Date::from(date.to<calendars::IslamicDate>());
    case ConversionTarget::HEBREW:
      return calendars::Date::from(date.to<calendars::HebrewDate>());
    case ConversionTarget::OLD_HINDU_SOLAR:
      return calendars::Date::from(date.to<calendars::OldHinduSolarDate>());
    case ConversionTarget::OLD_HINDU_LUNI_SOLAR:
      return calendars::Date::from(date.to<calendars::OldHinduLuniSolarDate>());
    case ConversionTarget::MAYAN_LONG_DATE:
      return calendars::Date::from(date.to<calendars::mayan::LongDate>());
  }
  return date;
}

constexpr auto daysOfWeek = std::array{
    calendars::DAY_OF_WEEK::SUNDAY,
    calendars::DAY_OF_WEEK::MONDAY,
    calendars::DAY_OF_WEEK::TUESDAY,
    calendars::DAY_OF_WEEK::WEDNESDAY,
    calendars::DAY_OF_WEEK::THURSDAY,
    calendars::DAY_OF_WEEK::FRIDAY,
    calendars::DAY_OF_WEEK::SATURDAY,
};

constexpr std::string_view day_of_week_name(calendars::DAY_OF_WEEK name) {
#define CASE(NAME) case calendars::DAY_OF_WEEK::NAME: return #NAME;
  switch (name) {
    CASE(SUNDAY)
    CASE(MONDAY)
    CASE(TUESDAY)
    CASE(WEDNESDAY)
    CASE(THURSDAY)
    CASE(FRIDAY)
    CASE(SATURDAY)
  }
#undef CASE
}
