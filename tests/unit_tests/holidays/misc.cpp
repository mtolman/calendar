#include "common.h"
#include "src/calendars.h"
#include "src/holidays.h"

namespace h = calendars::holidays;
namespace c = calendars;

TEST_SUITE("Misc Holidays") {
  TEST_CASE("Unlucky Friday") {
    auto year    = 2018;
    auto fridays = h::misc::unlucky_fridays_in_year(year);
    for (decltype(auto) friday : fridays) {
      REQUIRE_EQ(friday.day_of_week(), c::DAY_OF_WEEK::FRIDAY);
      REQUIRE_EQ(friday.to<c::GregorianDate>().day, 13);
    }
  }

  TEST_SUITE("Sample Data") {
    TEST_CASE("Unlucky Fridays") {
      using D = calendars::Date;
      using G = calendars::GregorianDate;

      auto expected = std::vector<D>{
          D{G{2020, G::MONTH::MARCH, 13}},
          D{G{2020, G::MONTH::NOVEMBER, 13}},
      };

      auto actual = calendars::holidays::misc::unlucky_fridays_in_year(2020);
      REQUIRE_EQ(expected.size(), actual.size());

      for (size_t i = 0; i < expected.size(); ++i) {
        REQUIRE_EQ(expected[ i ], actual[ i ]);
      }
    }
  }
}
