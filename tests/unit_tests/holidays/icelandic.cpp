#include "common.h"
#include "src/calendars/icelandic_date.h"
#include "src/calendars/gregorian_date.h"

TEST_SUITE("Icelandic") {
  namespace c = calendars;
  TEST_SUITE("Sample Data") {
    TEST_CASE("Icelandic Summer") {
      const auto expectedDays = std::vector<int16_t>{
          20, 19, 25, 24, 22, 21, 20, 19, 24, 23, 22, 21, 19, 25, 24, 23, 21, 20, 19, 25, 23, 22, 21, 20, 25, 24, 23, 22, 20, 19, 25, 24, 22, 21, 20,
          19, 24, 23, 22, 21, 19, 25, 24, 23, 21, 20, 19, 25, 23, 22, 21, 20, 25, 24, 23, 22, 20, 19, 25, 24, 22, 21, 20, 19, 24, 23, 22, 21, 19, 25,
          24, 23, 21, 20, 19, 25, 23, 22, 21, 20, 25, 24, 23, 22, 20, 19, 25, 24, 22, 21, 20, 19, 24, 23, 22, 21, 19, 25, 24, 23, 22, 21, 20, 19};
      for (size_t i = 0; i < expectedDays.size(); ++i) {
        REQUIRE_EQ(c::Date{c::IcelandicDate::summer(sampleYears[ i ])}, c::Date{c::GregorianDate{sampleYears[ i ], c::GregorianDate::MONTH::APRIL, expectedDays[ i ]}});
      }
    }

    TEST_CASE("Icelandic Winter") {
      const auto expectedDays = std::vector<int16_t>{
          21, 27, 26, 25, 23, 22, 21, 27, 25, 24, 23, 22, 27, 26, 25, 24, 22, 21, 27, 26, 24, 23, 22, 28, 26, 25, 24, 23, 21, 27, 26, 25, 23, 22, 21,
          27, 25, 24, 23, 22, 27, 26, 25, 24, 22, 21, 27, 26, 24, 23, 22, 28, 26, 25, 24, 23, 21, 27, 26, 25, 23, 22, 21, 27, 25, 24, 23, 22, 27, 26,
          25, 24, 22, 21, 27, 26, 24, 23, 22, 28, 26, 25, 24, 23, 21, 27, 26, 25, 23, 22, 21, 27, 25, 24, 23, 22, 27, 26, 25, 24, 23, 22, 21, 27};
      for (size_t i = 0; i < expectedDays.size(); ++i) {
        REQUIRE_EQ(c::Date{c::IcelandicDate::winter(sampleYears[ i ])}, c::Date{c::GregorianDate{sampleYears[ i ], c::GregorianDate::MONTH::OCTOBER, expectedDays[ i ]}});
      }
    }
  }
}
