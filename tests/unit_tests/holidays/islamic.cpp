#include <doctest.h>

#include "src/calendars.h"
#include "src/holidays.h"

namespace h = calendars::holidays;
namespace c = calendars;

TEST_SUITE("Islamic") {
  TEST_CASE("New Year") {
    using D = c::IslamicDate;

    [](int64_t year) {
      const auto date = h::islamic::new_year(year);
      CHECK_EQ(date, c::Date{D{year, D::MONTH::MUHARRAM, 1}});
    }(2021);

    [](int64_t year) {
      const auto dates = h::islamic::gregorian::new_year(year);
      for (auto d : dates) {
        auto date = d.to<D>();
        CHECK_EQ(date.month, D::MONTH::MUHARRAM);
        CHECK_EQ(date.day, 1);
      }
    }(2021);
  }

  TEST_CASE("Ashura") {
    using D = c::IslamicDate;

    [](int64_t year) {
      const auto date = h::islamic::ashura(year);
      CHECK_EQ(date, c::Date{D{year, D::MONTH::MUHARRAM, 10}});
    }(2021);

    [](int64_t year) {
      const auto dates = h::islamic::gregorian::ashura(year);
      for (auto d : dates) {
        auto date = d.to<D>();
        CHECK_EQ(date.month, D::MONTH::MUHARRAM);
        CHECK_EQ(date.day, 10);
      }
    }(2021);
  }

  TEST_CASE("Mawlid") {
    using D = c::IslamicDate;

    [](int64_t year) {
      const auto date = h::islamic::mawlid(year);
      CHECK_EQ(date, c::Date{D{year, D::MONTH::RABI_I, 12}});
    }(2021);

    [](int64_t year) {
      const auto dates = h::islamic::gregorian::mawlid(year);
      for (auto d : dates) {
        auto date = d.to<D>();
        CHECK_EQ(date.month, D::MONTH::RABI_I);
        CHECK_EQ(date.day, 12);
      }
    }(2021);
  }

  TEST_CASE("lailat_al_miraj") {
    using D = c::IslamicDate;

    [](int64_t year) {
      const auto date = h::islamic::lailat_al_miraj(year);
      CHECK_EQ(date, c::Date{D{year, D::MONTH::RAJAB, 27}});
    }(2021);

    [](int64_t year) {
      const auto dates = h::islamic::gregorian::lailat_al_miraj(year);
      for (auto d : dates) {
        auto date = d.to<D>();
        CHECK_EQ(date.month, D::MONTH::RAJAB);
        CHECK_EQ(date.day, 27);
      }
    }(2021);
  }

  TEST_CASE("lailat_al_baraa") {
    using D = c::IslamicDate;

    [](int64_t year) {
      const auto date = h::islamic::lailat_al_baraa(year);
      CHECK_EQ(date, c::Date{D{year, D::MONTH::SHABAN, 15}});
    }(2021);

    [](int64_t year) {
      const auto dates = h::islamic::gregorian::lailat_al_baraa(year);
      for (auto d : dates) {
        auto date = d.to<D>();
        CHECK_EQ(date.month, D::MONTH::SHABAN);
        CHECK_EQ(date.day, 15);
      }
    }(2021);
  }

  TEST_CASE("ramadan") {
    using D = c::IslamicDate;

    [](int64_t year) {
      const auto date = h::islamic::ramadan(year);
      CHECK_EQ(date, c::Date{D{year, D::MONTH::RAMADAN, 1}});
    }(2021);

    [](int64_t year) {
      const auto dates = h::islamic::gregorian::ramadan(year);
      for (auto d : dates) {
        auto date = d.to<D>();
        CHECK_EQ(date.month, D::MONTH::RAMADAN);
        CHECK_EQ(date.day, 1);
      }
    }(2021);
  }

  TEST_CASE("lailat_al_kadr") {
    using D = c::IslamicDate;

    [](int64_t year) {
      const auto date = h::islamic::lailat_al_kadr(year);
      CHECK_EQ(date, c::Date{D{year, D::MONTH::RAMADAN, 27}});
    }(2021);

    [](int64_t year) {
      const auto dates = h::islamic::gregorian::lailat_al_kadr(year);
      for (auto d : dates) {
        auto date = d.to<D>();
        CHECK_EQ(date.month, D::MONTH::RAMADAN);
        CHECK_EQ(date.day, 27);
      }
    }(2021);
  }

  TEST_CASE("eid_ul_fitr") {
    using D = c::IslamicDate;

    [](int64_t year) {
      const auto date = h::islamic::eid_ul_fitr(year);
      CHECK_EQ(date, c::Date{D{year, D::MONTH::SHAWWAL, 1}});
    }(2021);

    [](int64_t year) {
      const auto dates = h::islamic::gregorian::eid_ul_fitr(year);
      for (auto d : dates) {
        auto date = d.to<D>();
        CHECK_EQ(date.month, D::MONTH::SHAWWAL);
        CHECK_EQ(date.day, 1);
      }
    }(2021);
  }

  TEST_CASE("eid_ul_adha unit") {
    using D = c::IslamicDate;

    [](int64_t year) {
      const auto date = h::islamic::eid_ul_adha(year);
      CHECK_EQ(date, c::Date{D{year, D::MONTH::DHU_AL_HIJJA, 10}});
    }(2021);

    const auto testGreg = [](int32_t year) {
      const auto dates = h::islamic::gregorian::eid_ul_adha(year);
      for (auto d : dates) {
        auto date = d.to<D>();
        CHECK_EQ(date.month, D::MONTH::DHU_AL_HIJJA);
        CHECK_EQ(date.day, 10);
      }
    };

    testGreg(2021);
  }
}
