#include <doctest.h>

#include "src/calendars/balinese_pawukon_date.h"
#include "common.h"
#include "src/calendars/julian_day.h"
#include <iostream>
#include <array>

TEST_SUITE("Balinese Pawukon") {
  TEST_CASE("Epoch") {
    CHECK_EQ(calendars::BalinesePawukonDate::epoch, calendars::Date::from(calendars::JulianDay{146}).day);
  }

  TEST_CASE("Equals") {
    CHECK_EQ(calendars::Date{24}.to<calendars::BalinesePawukonDate>(), calendars::Date{24}.to<calendars::BalinesePawukonDate>());

    // Test that the rest of the 209 days in the 210-dat cycle don't equal the original day
    for (int i = 0; i < 209; ++i) {
      CHECK_NE(calendars::Date{24}.to<calendars::BalinesePawukonDate>(), calendars::Date{25 + i}.to<calendars::BalinesePawukonDate>());
    }
  }

  TEST_CASE("Sample Data") {
    using T = calendars::BalinesePawukonDate;

    constexpr auto f = false;
    constexpr auto t = true;
    int line = __LINE__;
    static std::vector<T> expected = {
        T{f, 1, 1, 1, 3, 1, 1, 5, 7, 3},
        T{t, 2, 2, 1, 4, 5, 4, 5, 5, 2},
        T{t, 2, 2, 1, 5, 5, 4, 1, 5, 6},
        T{f, 1, 2, 3, 3, 5, 1, 3, 5, 3},
        T{f, 1, 1, 3, 3, 1, 4, 3, 1, 5},
        T{t, 2, 2, 1, 1, 5, 2, 1, 8, 0},
        T{f, 1, 2, 3, 3, 5, 7, 3, 2, 7},
        T{f, 1, 2, 2, 1, 2, 1, 2, 2, 1},
        T{f, 1, 2, 1, 1, 5, 1, 1, 8, 1},
        T{t, 2, 3, 1, 1, 3, 6, 1, 3, 2},
        T{f, 1, 1, 1, 1, 1, 7, 5, 1, 5},
        T{t, 2, 3, 4, 1, 6, 6, 8, 6, 2},
        T{f, 1, 2, 3, 3, 5, 1, 3, 5, 3},
        T{f, 1, 1, 4, 1, 4, 1, 4, 7, 1},
        T{f, 1, 2, 2, 2, 2, 4, 2, 5, 7},
        T{f, 1, 2, 4, 2, 2, 7, 8, 8, 9},
        T{t, 2, 1, 4, 4, 4, 7, 4, 7, 4},
        T{f, 1, 2, 3, 3, 5, 7, 3, 2, 7},
        T{f, 1, 3, 4, 2, 6, 4, 8, 3, 7},
        T{t, 2, 2, 4, 5, 2, 1, 4, 5, 4},
        T{t, 2, 1, 2, 2, 4, 6, 2, 1, 6},
        T{t, 2, 2, 4, 5, 2, 1, 4, 5, 4},
        T{f, 1, 3, 4, 5, 6, 2, 8, 3, 3},
        T{f, 1, 1, 1, 2, 1, 4, 5, 4, 7},
        T{t, 2, 2, 1, 5, 5, 1, 5, 8, 4},
        T{t, 2, 3, 2, 5, 6, 1, 2, 3, 4},
        T{t, 2, 2, 2, 3, 2, 2, 2, 1, 2},
        T{f, 1, 2, 3, 5, 5, 2, 3, 2, 3},
        T{t, 2, 2, 4, 1, 2, 5, 4, 8, 4},
        T{t, 2, 2, 2, 5, 2, 3, 2, 8, 2},
        T{t, 2, 2, 4, 5, 2, 1, 4, 5, 4},
        T{t, 2, 1, 3, 4, 1, 4, 7, 1, 2},
        T{f, 1, 3, 4, 3, 6, 1, 8, 6, 3}
    };

    CHECK_EQ(expected.size(), sampleRdDates.size());

    for (size_t i = 0; i < expected.size(); ++i) {
      if (sampleRdDates[ i ].to<T>() != expected[ i ]) {
        const auto printBDate = [](const calendars::BalinesePawukonDate& date) {
          const auto vals = std::array {
            static_cast<int>(date.dwiwara), static_cast<int>(date.triwara), static_cast<int>(date.caturwara),
                static_cast<int>(date.pancawara), static_cast<int>(date.sadwara), static_cast<int>(date.saptawara),
                static_cast<int>(date.asatawara), static_cast<int>(date.sangawara), static_cast<int>(date.dasawara),
          };
          if (date.luang) {
            std::cout << "f ";
          }
          else {
            std::cout << "t ";
          }
          for (const auto& v : vals) {
            std::cout << v << " ";
          }
        };
        std::cout << "Error with line " << (line + 2 + i) << "! Expected: ";
        printBDate(expected[i]);
        std::cout << "Received: ";
        printBDate(sampleRdDates[ i ].to<T>());
        std::cout << "\n";
      }
      CHECK_EQ(sampleRdDates[ i ].to<T>(), expected[ i ]);
    }
  }
}
