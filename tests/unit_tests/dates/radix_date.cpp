#include <doctest.h>

#include "src/calendars/radix_date.h"
#include "common.h"
#include "common_test_headers.h"

TEST_SUITE("Radix Date") {
  TEST_CASE("Basic") {
    CHECK_IN_RANGE_DEF(calendars::Moment::from(calendars::RadixDate<1, 3, 7, 24, 60, 60>{4, 1, 12, 44, 2.88}).datetime, 29.5305888889);

    auto result = calendars::Moment{29.5305888889}.to<calendars::RadixDate<1, 3, 7, 24, 60, 60>>();
    CHECK_EQ(result[ 0 ], 4);
    CHECK_EQ(result[ 1 ], 1);
    CHECK_EQ(result[ 2 ], 12);
    CHECK_EQ(result[ 3 ], 44);
    CHECK_IN_RANGE(result[ 4 ], 2.88, 0.00001);
  }

  TEST_CASE("No Int Segment") {
    CHECK_IN_RANGE_DEF(calendars::Moment::from(calendars::RadixDate<0, 3, 24, 60, 60>{1, 12, 44, 2.88}).datetime, 1.5305888889);

    auto result = calendars::Moment{1.5305888889}.to<calendars::RadixDate<0, 3, 24, 60, 60>>();
    CHECK_EQ(result[ 0 ], 1);
    CHECK_EQ(result[ 1 ], 12);
    CHECK_EQ(result[ 2 ], 44);
    CHECK_IN_RANGE(result[ 3 ], 2.88, 0.00001);
  }

  TEST_CASE("Frac Segment") {
    CHECK_IN_RANGE_DEF(calendars::Moment::from(calendars::RadixDate<1, 2, 7, 60, 60>{4, 1, 30, 15}).datetime, 29.5041666667);
    CHECK_IN_RANGE_DEF(calendars::Moment::from(calendars::RadixDate<1, 2, 7, 60, 60>{1, 0, 30, 15}).datetime, 7.5041666667);
    CHECK_IN_RANGE_DEF(calendars::Moment::from(calendars::RadixDate<1, 2, 7, 60, 60>{0, 0, 30, 15}).datetime, 0.5041666667);

    auto result = calendars::Moment{29}.to<calendars::RadixDate<1, 0, 7>>();
    CHECK_EQ(result[ 0 ], 4);
    CHECK_EQ(result[ 1 ], 1);
  }

  TEST_CASE("No Frac Segment") {
    CHECK_IN_RANGE_DEF(calendars::Moment::from(calendars::RadixDate<1, 0, 7>{4, 1}).datetime, 29);
    CHECK_IN_RANGE_DEF(calendars::Moment::from(calendars::RadixDate<1, 0, 7>{1, 0}).datetime, 7);
    CHECK_IN_RANGE_DEF(calendars::Moment::from(calendars::RadixDate<1, 0, 7>{0, 0}).datetime, 0);

    auto result = calendars::Moment{29}.to<calendars::RadixDate<1, 0, 7>>();
    CHECK_EQ(result[ 0 ], 4);
    CHECK_EQ(result[ 1 ], 1);
  }
}
