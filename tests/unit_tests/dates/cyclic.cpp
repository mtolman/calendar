#include <doctest.h>

#include "src/calendars/cyclic.h"
#include "common.h"

TEST_SUITE("Cyclic Date") {
  TEST_SUITE("Single Cycle") {
    TEST_SUITE("Strictly Before") {
      // Recreate the coptic calendar using our generic single-cycle calendar
      using CopticCyclic = calendars::SingleCycle<calendars::SingleCycleType::STRICTLY_BEFORE, 103605, (365 * 4) + 1, 30, 4, 1, 1, 4>;

      TEST_CASE("Comparisons") { comp_tests_three_args<CopticCyclic>(); }

      TEST_CASE("Conversions") {
        CopticCyclic    date;
        calendars::Date rdate;

        rdate.day = 8849;
        date      = rdate.to<CopticCyclic>();
        rdate     = calendars::Date::from(date);
        CHECK_EQ(rdate.day, 8849);
      }

      TEST_CASE("Sample Data") {
        using T = CopticCyclic;

        static std::vector<T> expected = {T{-870, 12, 6}, T{-451, 4, 12},  T{-213, 1, 29},  T{-148, 2, 5},   T{186, 5, 12},   T{292, 9, 23},
                                          T{411, 3, 11},  T{729, 8, 24},   T{812, 9, 23},   T{906, 7, 20},   T{956, 7, 7},    T{1004, 7, 30},
                                          T{1014, 8, 25}, T{1107, 10, 10}, T{1152, 5, 29},  T{1208, 8, 5},   T{1270, 1, 12},  T{1276, 6, 29},
                                          T{1364, 10, 6}, T{1396, 10, 26}, T{1432, 11, 19}, T{1484, 10, 14}, T{1535, 11, 27}, T{1555, 7, 19},
                                          T{1619, 8, 11}, T{1645, 12, 19}, T{1658, 1, 19},  T{1659, 8, 11},  T{1660, 1, 26},  T{1708, 7, 8},
                                          T{1712, 6, 17}, T{1755, 3, 1},   T{1810, 11, 11}};

        CHECK_EQ(expected.size(), sampleRdDates.size());

        for (size_t i = 0; i < expected.size(); ++i) {
          CHECK_EQ(sampleRdDates[ i ].to<T>(), expected[ i ]);
          CHECK_EQ(calendars::Date::from(expected[ i ]), sampleRdDates[ i ]);
        }
      }
      static_assert(CopticCyclic::epoch == 103605);
    }

    TEST_SUITE("Up To And Including") {
      // Recreate the coptic calendar using our generic single-cycle calendar
      using CopticCyclic = calendars::SingleCycle<calendars::SingleCycleType::UP_TO_AND_INCLUDING, 103605, (365 * 4) + 1, 30, 4, 1, 1, 2>;

      TEST_CASE("Comparisons") { comp_tests_three_args<CopticCyclic>(); }

      TEST_CASE("Conversions") {
        CopticCyclic    date;
        calendars::Date rdate;

        rdate.day = 8849;
        date      = rdate.to<CopticCyclic>();
        rdate     = calendars::Date::from(date);
        CHECK_EQ(rdate.day, 8849);
      }

      TEST_CASE("Sample Data") {
        using T = CopticCyclic;

        static std::vector<T> expected = {T{-870, 12, 6}, T{-451, 4, 12},  T{-213, 1, 29},  T{-148, 2, 5},   T{186, 5, 12},   T{292, 9, 23},
                                          T{411, 3, 11},  T{729, 8, 24},   T{812, 9, 23},   T{906, 7, 20},   T{956, 7, 7},    T{1004, 7, 30},
                                          T{1014, 8, 25}, T{1107, 10, 10}, T{1152, 5, 29},  T{1208, 8, 5},   T{1270, 1, 12},  T{1276, 6, 29},
                                          T{1364, 10, 6}, T{1396, 10, 26}, T{1432, 11, 19}, T{1484, 10, 14}, T{1535, 11, 27}, T{1555, 7, 19},
                                          T{1619, 8, 11}, T{1645, 12, 19}, T{1658, 1, 19},  T{1659, 8, 11},  T{1660, 1, 26},  T{1708, 7, 8},
                                          T{1712, 6, 17}, T{1755, 3, 1},   T{1810, 11, 11}};

        CHECK_EQ(expected.size(), sampleRdDates.size());

        for (size_t i = 0; i < expected.size(); ++i) {
          CHECK_EQ(sampleRdDates[ i ].to<T>(), expected[ i ]);
          CHECK_EQ(calendars::Date::from(expected[ i ]), sampleRdDates[ i ]);
        }
      }
      static_assert(CopticCyclic::epoch == 103605);
    }

    TEST_SUITE("Mean Month") {
      // Recreate the coptic calendar using our generic single-cycle calendar
      using Cyclic = calendars::SingleCycle<calendars::SingleCycleType::MEAN_MONTH, 103605, 360, 30, 4, 1>;

      TEST_CASE("Comparisons") { comp_tests_three_args<Cyclic>(); }

      TEST_CASE("Conversions") {
        Cyclic    date;
        calendars::Date rdate;

        rdate.day = 8849;
        date      = rdate.to<Cyclic>();
        rdate     = calendars::Date::from(date);
        CHECK_EQ(rdate.day, 8849);
      }
    }

    TEST_SUITE("At or Before Mean Month") {
      // Recreate the coptic calendar using our generic single-cycle calendar
      using Cyclic = calendars::SingleCycle<calendars::SingleCycleType::AT_OR_BEFORE_MEAN_MONTH, 103605, 360, 30, 4, 1>;

      TEST_CASE("Comparisons") { comp_tests_three_args<Cyclic>(); }

      TEST_CASE("Conversions") {
        Cyclic    date;
        calendars::Date rdate;

        rdate.day = 8849;
        date      = rdate.to<Cyclic>();
        rdate     = calendars::Date::from(date);
        CHECK_EQ(rdate.day, 8849);
      }
    }
  }

//  TEST_SUITE("Double Cycle") {
//    TEST_SUITE("Strictly Before") {
//      // Recreate the coptic calendar using our generic single-cycle calendar
//      using Cyclic = calendars::DoubleCycle<calendars::DoubleCycleType::STRICTLY_BEFORE, 103605, 360, 30, 4, 1>;
//
//      TEST_CASE("Comparisons") { comp_tests_three_args<Cyclic>(); }
//
//      TEST_CASE("Conversions") {
//        Cyclic    date;
//        calendars::Date rdate;
//
//        rdate.day = 8849;
//        date      = rdate.to<Cyclic>();
//        rdate     = calendars::Date::from(date);
//        CHECK_EQ(rdate.day, 8849);
//      }
//    }
//    TEST_SUITE("At or Before") {
//      // Recreate the coptic calendar using our generic single-cycle calendar
//      using Cyclic = calendars::DoubleCycle<calendars::DoubleCycleType::AT_OR_BEFORE, 103605, 360, 30, 4, 1>;
//
//      TEST_CASE("Comparisons") { comp_tests_three_args<Cyclic>(); }
//
//      TEST_CASE("Conversions") {
//        Cyclic    date;
//        calendars::Date rdate;
//
//        rdate.day = 8849;
//        date      = rdate.to<Cyclic>();
//        rdate     = calendars::Date::from(date);
//        CHECK_EQ(rdate.day, 8849);
//      }
//    }
//  }
}
