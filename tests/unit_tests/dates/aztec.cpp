#include <doctest.h>

#include "src/calendars/aztec.h"
#include "common.h"
#include "src/calendars/julian_date.h"

TEST_SUITE("Aztec") {
  TEST_CASE("Xihuitl") {
    SUBCASE("Comparisons") { eq_tests_two_args<calendars::aztec::Xihuitl, calendars::aztec::Xihuitl::MONTH, int16_t>(); }

    SUBCASE("Sample Data") {
      using T = calendars::aztec::Xihuitl;

      static std::vector<std::vector<int16_t>> nums = {{2, 6},   {14, 2},  {13, 8},  {14, 11}, {5, 6},   {13, 4}, {5, 1},  {17, 4},  {1, 9},
                                                       {17, 14}, {17, 14}, {1, 4},   {2, 11},  {5, 19},  {18, 5}, {3, 20}, {12, 17}, {3, 1},
                                                       {8, 20},  {10, 8},  {11, 20}, {10, 18}, {13, 13}, {7, 10}, {9, 8},  {16, 3},  {18, 1},
                                                       {9, 18},  {18, 9},  {8, 18},  {7, 18},  {3, 2},   {16, 6}};

      static std::vector<T> expected = {};
      expected.reserve(nums.size());
      for (const auto& date : nums) {
        expected.emplace_back(T{static_cast<T::MONTH>(date[ 0 ]), date[ 1 ]});
      }

      CHECK_EQ(expected.size(), sampleRdDates.size());

      for (size_t i = 0; i < expected.size(); ++i) {
        CHECK_EQ(sampleRdDates[ i ].to<T>(), expected[ i ]);
      }
    }
  }

  TEST_CASE("Tonalpohualli") {
    SUBCASE("Comparisons") { eq_tests_two_args<calendars::aztec::Tonalpohualli, int16_t, calendars::aztec::Tonalpohualli::NAME>(); }

    SUBCASE("Sample Data") {
      using T = calendars::aztec::Tonalpohualli;

      static std::vector<std::vector<int16_t>> nums = {{5, 9},  {9, 15}, {12, 11}, {9, 19},  {3, 9},  {7, 17}, {2, 9},   {4, 2},  {7, 7},
                                                       {9, 17}, {7, 7},  {12, 2},  {10, 19}, {2, 12}, {6, 18}, {12, 18}, {3, 20}, {9, 19},
                                                       {8, 18}, {3, 6},  {6, 18},  {10, 16}, {12, 6}, {13, 3}, {11, 1},  {3, 6},  {1, 4},
                                                       {9, 11}, {11, 2}, {12, 16}, {9, 16},  {8, 15}, {2, 14}};

      static std::vector<T> expected = {};
      expected.reserve(nums.size());
      for (const auto& date : nums) {
        expected.emplace_back(T{date[ 0 ], static_cast<T::NAME>(date[ 1 ])});
      }

      CHECK_EQ(expected.size(), sampleRdDates.size());

      for (size_t i = 0; i < expected.size(); ++i) {
        //      auto actual = sampleRdDates[ i ].to<T>();
        //      if (sampleRdDates[ i ].to<T>() != expected[ i ]) {
        //        printf(
        //            "TO conversion failed! Index: %zu. Actual<%d, %d>; "
        //            "Expected<%d, %d>",
        //            i,
        //            static_cast<int>(actual.number),
        //            static_cast<int>(actual.name),
        //
        //            static_cast<int>(expected[i].number),
        //            static_cast<int>(expected[i].name)
        //        );
        //      }
        CHECK_EQ(sampleRdDates[ i ].to<T>(), expected[ i ]);
      }
    }
  }

  TEST_CASE("Xiuhomolpilli") {
    SUBCASE("Comparisons") {
      eq_tests_two_args<calendars::aztec::Xiuhomolpilli, int16_t, calendars::aztec::Xiuhomolpilli::NAME>();
    }
  }

  TEST_CASE("Correlation") {
    CHECK_EQ(calendars::aztec::details::correlation, calendars::JulianDate{1521, 8, 13}.to_date().day);
  }
}

