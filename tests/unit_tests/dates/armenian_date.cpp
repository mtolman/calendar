#include <doctest.h>

#include "src/calendars/armenian_date.h"
#include "common.h"

TEST_SUITE("Armenian Date") {
  TEST_CASE("Comparisons") {
      comp_tests_three_args<calendars::ArmenianDate>();
  }

  TEST_CASE("Conversions") {
    calendars::ArmenianDate date;
    calendars::Date       rdate;

    rdate.day = 8849;
    date      = rdate.to<calendars::ArmenianDate>();
    rdate     = calendars::Date::from(date);
    CHECK_EQ(rdate.day, 8849);
  }

  TEST_CASE("Sample Data") {
    using T = calendars::ArmenianDate;

    static std::vector<T> expected = {T{-1138, 4, 10}, T{-720, 12, 6}, T{-482, 11, 22}, T{-417, 12, 15}, T{-82, 6, 10},   T{24, 11, 18},
                                      T{143, 6, 5},    T{462, 2, 3},   T{545, 3, 23},   T{639, 2, 13},   T{689, 2, 13},   T{737, 3, 18},
                                      T{747, 4, 15},   T{840, 6, 23},  T{885, 2, 24},   T{941, 5, 14},   T{1002, 11, 11}, T{1009, 4, 25},
                                      T{1097, 8, 24},  T{1129, 9, 22}, T{1165, 10, 24}, T{1217, 10, 2},  T{1268, 11, 27}, T{1288, 7, 24},
                                      T{1352, 9, 2},   T{1379, 1, 12}, T{1391, 2, 20},  T{1392, 9, 12},  T{1393, 2, 28},  T{1441, 8, 22},
                                      T{1445, 8, 2},   T{1488, 4, 26}, T{1544, 1, 15}};

    CHECK_EQ(expected.size(), sampleRdDates.size());

    for (size_t i = 0; i < expected.size(); ++i) {
      CHECK_EQ(sampleRdDates[ i ].to<T>(), expected[ i ]);
      CHECK_EQ(calendars::Date::from(expected[ i ]), sampleRdDates[ i ]);
    }
  }
}

static_assert(calendars::ArmenianDate::epoch == 201443);
