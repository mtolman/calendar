#include <doctest.h>

#include "src/calendars/ethiopian_date.h"
#include "common.h"

TEST_SUITE("Ethiopian Date") {
  TEST_CASE("Comparisons") {
    comp_tests_three_args<calendars::EthiopianDate>();
  }

  TEST_CASE("Conversions") {
    calendars::EthiopianDate date;
    calendars::Date        rdate;

    rdate.day = 8849;
    date      = rdate.to<calendars::EthiopianDate>();
    rdate     = calendars::Date::from(date);
    CHECK_EQ(rdate.day, 8849);
  }

  TEST_CASE("Sample Data") {
    using T = calendars::EthiopianDate;

    static std::vector<T> expected = {
        {-594, 12 ,6},
        {-175, 4, 12},
        {63, 1, 29},
        {128, 2, 5},
        {462, 5, 12},
        {568, 9, 23},
        {687, 3, 11},
        {1005, 8, 24},
        {1088, 9, 23},
        {1182, 7, 20},
        {1232, 7, 7},
        {1280, 7, 30},
        {1290, 8, 25},
        {1383, 10, 10},
        {1428, 5, 29},
        {1484, 8, 5},
        {1546, 1, 12},
        {1552, 6, 29},
        {1640, 10, 6},
        {1672, 10, 26},
        {1708, 11, 19},
        {1760, 10, 14},
        {1811, 11, 27},
        {1831, 7, 19},
        {1895, 8, 11},
        {1921, 12, 19},
        {1934, 1, 19},
        {1935, 8, 11},
        {1936, 1, 26},
        {1984, 7, 8},
        {1988, 6, 17},
        {2031, 3, 1},
        {2086, 11, 11}
    };

    CHECK_EQ(expected.size(), sampleRdDates.size());

    for (size_t i = 0; i < expected.size(); ++i) {
      CHECK_EQ(sampleRdDates[ i ].to<T>(), expected[ i ]);
      CHECK_EQ(calendars::Date::from(expected[ i ]), sampleRdDates[ i ]);
    }
  }
}

static_assert(calendars::EthiopianDate::epoch == 2796);
