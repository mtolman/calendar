#include <doctest.h>

#include "src/calendars/akan_day.h"
#include "common.h"

TEST_SUITE("Akan Day") {
  TEST_CASE("Constructors") {
    auto akan = calendars::AkanDay{4, 5};
    CHECK_EQ(akan.prefix, 4);
    CHECK_EQ(akan.stem, 5);

    akan = calendars::AkanDay{6, 7};
    CHECK_EQ(akan.prefix, 6);
    CHECK_EQ(akan.stem, 7);

    akan = calendars::AkanDay{42};
    CHECK_EQ(akan.prefix, 6);
    CHECK_EQ(akan.stem, 7);

    akan = calendars::AkanDay{31};
    CHECK_EQ(akan.prefix, 1);
    CHECK_EQ(akan.stem, 3);

    akan = calendars::AkanDay{static_cast<int64_t>(42)};
    CHECK_EQ(akan.prefix, 6);
    CHECK_EQ(akan.stem, 7);
  }

  TEST_CASE("On or Before") {
    auto akan = calendars::AkanDay{1, 1};
    auto rd   = calendars::Date{38};

    CHECK_EQ(akan_day_on_or_before(rd, akan), rd);

    rd = calendars::Date{37};
    CHECK_EQ(akan_day_on_or_before(rd, akan), calendars::Date{-4});
  }

  TEST_CASE("Equality") {
    eq_tests_two_args<calendars::AkanDay>();
  }

  TEST_CASE("Sample Data") {
    using T = calendars::AkanDay;

    static std::vector<T> expected = {T{6, 5}, T{4, 1}, T{4, 1}, T{4, 5}, T{6, 1}, T{4, 6}, T{4, 4}, T{1, 5}, T{4, 5}, T{2, 3}, T{6, 4},
                                      T{5, 3}, T{4, 5}, T{3, 5}, T{1, 1}, T{1, 4}, T{3, 4}, T{4, 4}, T{5, 1}, T{1, 5}, T{3, 3}, T{1, 5},
                                      T{5, 6}, T{6, 1}, T{4, 5}, T{5, 5}, T{1, 6}, T{4, 6}, T{1, 2}, T{1, 7}, T{1, 5}, T{6, 1}, T{5, 5}};

    CHECK_EQ(expected.size(), sampleRdDates.size());

    for (size_t i = 0; i < expected.size(); ++i) {
      CHECK_EQ(sampleRdDates[ i ].to<T>(), expected[ i ]);
    }
  }
}

static_assert(calendars::AkanDay::epoch == 37);
