#include <doctest.h>

#include "src/calendars/iso_date.h"
#include "common.h"

TEST_SUITE("Iso Date") {
  TEST_CASE("Comparisons") {
    comp_tests_three_args<calendars::IsoDate>();
  }

  TEST_CASE("Conversions") {
    calendars::IsoDate date{};
    calendars::Date  rdate;

    rdate.day = 8849;
    date      = rdate.to<calendars::IsoDate>();
    rdate     = calendars::Date::from(date);
    CHECK_EQ(rdate.day, 8849);
  }

  TEST_CASE("Sample Data") {
    using T = calendars::IsoDate;

    static std::vector<T> expected = {
        {-586, 29, 7},
        {-168, 49, 3},
        {70, 39, 3},
        {135, 39, 7},
        {470, 2, 3},
        {576, 21, 1},
        {694, 45, 6},
        {1013, 16, 7},
        {1096, 21, 7},
        {1190, 12, 5},
        {1240, 10, 6},
        {1288, 14, 5},
        {1298, 17, 7},
        {1391, 23, 7},
        {1436, 5, 3},
        {1492, 14, 6},
        {1553, 38, 6},
        {1560, 9, 6},
        {1648, 24, 3},
        {1680, 26, 7},
        {1716, 30, 5},
        {1768, 24, 7},
        {1819, 31, 1},
        {1839, 13, 3},
        {1903, 16, 7},
        {1929, 34, 7},
        {1941, 40, 1},
        {1943, 16, 1},
        {1943, 40, 4},
        {1992, 12, 2},
        {1996, 8, 7},
        {2038, 45, 3},
        {2094, 28, 7}
    };

    CHECK_EQ(expected.size(), sampleRdDates.size());

    for (size_t i = 0; i < expected.size(); ++i) {
      CHECK_EQ(sampleRdDates[ i ].to<T>(), expected[ i ]);
      CHECK_EQ(calendars::Date::from(expected[ i ]), sampleRdDates[ i ]);
    }
  }
}
