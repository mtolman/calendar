#include <doctest.h>

#include "src/calendars/mayan.h"
#include "src/calendars/julian_day.h"
#include "common.h"

TEST_SUITE("Mayan") {
  TEST_CASE("Long Date") {
    SUBCASE("Conversions") {
      CHECK_EQ(calendars::mayan::LongDate::epoch, calendars::Date::from(calendars::JulianDay{584283}).day);
      CHECK_EQ(calendars::mayan::Haab::epoch, calendars::mayan::LongDate::epoch - calendars::mayan::Haab{calendars::mayan::Haab::MONTH::CUMKU, 8}.ordinal());

      SUBCASE("mayan::LongDate") {
        calendars::mayan::LongDate date{};
        calendars::Date            rdate;

        rdate.day = 8849;
        date      = rdate.to<decltype(date)>();
        rdate     = calendars::Date::from(date);
        CHECK_EQ(rdate.day, 8849);
      }
    }

    SUBCASE("Sample Data") {
      using T = calendars::mayan::LongDate;

      static std::vector<T> expected = {
          T{{6, 8, 3, 13, 9}}, T{{7, 9, 8, 3, 15}}, T{{8, 1, 9, 8, 11}}, T{{8, 4, 15, 7, 19}},
          T{{9, 1, 14, 10, 9}}, T{{9, 7, 2, 8, 17}}, T{{9, 13, 2, 12, 9}}, T{{10, 9, 5, 14, 2}},
          T{{10, 13, 10, 1, 7}}, T{{10, 18, 5, 4, 17}}, T{{11, 0, 15, 17, 7}}, T{{11, 3, 4, 13, 2}},
          T{{11, 3, 14, 16, 19}}, T{{11, 8, 9, 7, 12}}, T{{11, 10, 14, 12, 18}}, T{{11, 13, 11, 12, 18}},
          T{{11, 16, 14, 1, 0}}, T{{11, 17, 0, 10, 19}}, T{{12, 1, 10, 2, 18}}, T{{12, 3, 2, 12, 6}},
          T{{12, 4, 19, 4, 18}}, T{{12, 7, 11, 16, 16}}, T{{12, 10, 3, 14, 6}}, T{{12, 11, 3, 13, 3}},
          T{{12, 14, 8, 13, 1}}, T{{12, 15, 15, 8, 6}}, T{{12, 16, 7, 13, 4}}, T{{12, 16, 9, 5, 11}},
          T{{12, 16, 9, 14, 2}}, T{{12, 18, 18, 16, 16}}, T{{12, 19, 2, 16, 16}}, T{{13, 1, 6, 4, 15}},
          T{{13, 4, 2, 13, 14}}
      };

      CHECK_EQ(expected.size(), sampleRdDates.size());

      for (size_t i = 0; i < expected.size(); ++i) {
        //            auto actual = sampleRdDates[ i ].to<T>();
        //            if (sampleRdDates[ i ].to<T>() != expected[ i ]) {
        //              printf(
        //                  "TO conversion failed! Index: %zu. Full Swing: %d, Actual<%d, %d, %d, %d, %d>; "
        //                  "Expected<%d, %d, %d, %d, %d>",
        //                  i,
        //                  calendars::Date::from(actual) == sampleRdDates[ i ],
        //                  static_cast<int>(actual.value[0]),
        //                  static_cast<int>(actual.value[1]),
        //                  static_cast<int>(actual.value[2]),
        //                  static_cast<int>(actual.value[3]),
        //                  static_cast<int>(actual.value[4]),
        //
        //                  static_cast<int>(expected[i].value[0]),
        //                  static_cast<int>(expected[i].value[1]),
        //                  static_cast<int>(expected[i].value[2]),
        //                  static_cast<int>(expected[i].value[3]),
        //                  static_cast<int>(expected[i].value[4])
        //              );
        //            }
        CHECK_EQ(sampleRdDates[ i ].to<T>(), expected[ i ]);
        //
        //            if (calendars::Date::from(expected[ i ]) != sampleRdDates[ i ]) {
        //              printf(
        //                  "FROM conversion failed! Index: %zu. Full Swing: %d, Actual<%d, %d, %d, %d, %d>; "
        //                  "Expected<%d, %d, %d, %d, %d>",
        //                  i,
        //                  calendars::Date::from(actual) == sampleRdDates[ i ],
        //                  static_cast<int>(actual.value[0]),
        //                  static_cast<int>(actual.value[1]),
        //                  static_cast<int>(actual.value[2]),
        //                  static_cast<int>(actual.value[3]),
        //                  static_cast<int>(actual.value[4]),
        //
        //                  static_cast<int>(expected[i].value[0]),
        //                  static_cast<int>(expected[i].value[1]),
        //                  static_cast<int>(expected[i].value[2]),
        //                  static_cast<int>(expected[i].value[3]),
        //                  static_cast<int>(expected[i].value[4]));
        //            }
        CHECK_EQ(calendars::Date::from(expected[ i ]), sampleRdDates[ i ]);
      }
    }
  }

  TEST_CASE("Haab") {
    SUBCASE("Sample Data") {
      using T = calendars::mayan::Haab;

      static std::vector<T> expected = {
          T{static_cast<T::MONTH>(11), 12}, T{static_cast<T::MONTH>(5), 3}, T{static_cast<T::MONTH>(4), 9},
          T{static_cast<T::MONTH>(5), 12}, T{static_cast<T::MONTH>(14), 12}, T{static_cast<T::MONTH>(4), 5},
          T{static_cast<T::MONTH>(14), 7}, T{static_cast<T::MONTH>(8), 5}, T{static_cast<T::MONTH>(10), 15},
          T{static_cast<T::MONTH>(8), 15}, T{static_cast<T::MONTH>(8), 15}, T{static_cast<T::MONTH>(10), 10},
          T{static_cast<T::MONTH>(11), 17}, T{static_cast<T::MONTH>(15), 5}, T{static_cast<T::MONTH>(9), 6},
          T{static_cast<T::MONTH>(13), 6}, T{static_cast<T::MONTH>(3), 18}, T{static_cast<T::MONTH>(12), 7},
          T{static_cast<T::MONTH>(18), 6}, T{static_cast<T::MONTH>(1), 9}, T{static_cast<T::MONTH>(3), 1},
          T{static_cast<T::MONTH>(1), 19}, T{static_cast<T::MONTH>(4), 14}, T{static_cast<T::MONTH>(16), 16},
          T{static_cast<T::MONTH>(18), 14}, T{static_cast<T::MONTH>(7), 4}, T{static_cast<T::MONTH>(9), 2},
          T{static_cast<T::MONTH>(19), 4}, T{static_cast<T::MONTH>(9), 10}, T{static_cast<T::MONTH>(18), 4},
          T{static_cast<T::MONTH>(17), 4}, T{static_cast<T::MONTH>(12), 8}, T{static_cast<T::MONTH>(7), 7}
      };

      CHECK_EQ(expected.size(), sampleRdDates.size());

      for (size_t i = 0; i < expected.size(); ++i) {
        CHECK_EQ(sampleRdDates[ i ].to<T>(), expected[ i ]);
      }
    }
  }

  TEST_CASE("Tzolkin") {
    SUBCASE("Sample Data") {

      using T = calendars::mayan::Tzolkin;

      static std::vector<T> expected = {
          T{5, static_cast<T::NAME>(9)}, T{9, static_cast<T::NAME>(15)}, T{12, static_cast<T::NAME>(11)},
          T{9, static_cast<T::NAME>(19)}, T{3, static_cast<T::NAME>(9)}, T{7, static_cast<T::NAME>(17)},
          T{2, static_cast<T::NAME>(9)}, T{4, static_cast<T::NAME>(2)}, T{7, static_cast<T::NAME>(7)},
          T{9, static_cast<T::NAME>(17)}, T{7, static_cast<T::NAME>(7)}, T{12, static_cast<T::NAME>(2)},
          T{10, static_cast<T::NAME>(19)}, T{2, static_cast<T::NAME>(12)}, T{6, static_cast<T::NAME>(18)},
          T{12, static_cast<T::NAME>(18)}, T{3, static_cast<T::NAME>(20)}, T{9, static_cast<T::NAME>(19)},
          T{8, static_cast<T::NAME>(18)}, T{3, static_cast<T::NAME>(6)}, T{6, static_cast<T::NAME>(18)},
          T{10, static_cast<T::NAME>(16)}, T{12, static_cast<T::NAME>(6)}, T{13, static_cast<T::NAME>(3)},
          T{11, static_cast<T::NAME>(1)}, T{3, static_cast<T::NAME>(6)}, T{1, static_cast<T::NAME>(4)},
          T{9, static_cast<T::NAME>(11)}, T{11, static_cast<T::NAME>(2)}, T{12, static_cast<T::NAME>(16)},
          T{9, static_cast<T::NAME>(16)}, T{8, static_cast<T::NAME>(15)}, T{2, static_cast<T::NAME>(14)},
      };

      CHECK_EQ(expected.size(), sampleRdDates.size());

      for (size_t i = 0; i < expected.size(); ++i) {
        CHECK_EQ(sampleRdDates[ i ].to<T>(), expected[ i ]);
      }
    }
  }
}

static_assert(calendars::mayan::LongDate::epoch == -1137142);
