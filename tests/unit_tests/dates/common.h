#pragma once

#include <array>
#include "src/calendars/common.h"

template<typename CLASS>
void comp_test_one_arg() {
  CHECK(CLASS{0} == CLASS{0});
  CHECK(CLASS{58} == CLASS{58});
  CHECK_FALSE(CLASS{12} == CLASS{58});
  CHECK_FALSE(CLASS{12} == CLASS{5});

  CHECK(CLASS{58} != CLASS{12});
  CHECK(CLASS{12} != CLASS{58});
  CHECK_FALSE(CLASS{58} != CLASS{58});

  CHECK(CLASS{12} < CLASS{58});
  CHECK_FALSE(CLASS{58} < CLASS{58});
  CHECK_FALSE(CLASS{60} < CLASS{58});

  CHECK(CLASS{58} > CLASS{12});
  CHECK_FALSE(CLASS{58} > CLASS{58});
  CHECK_FALSE(CLASS{58} > CLASS{60});

  CHECK(CLASS{12} <= CLASS{58});
  CHECK(CLASS{58} <= CLASS{58});
  CHECK_FALSE(CLASS{60} <= CLASS{58});
  CHECK(CLASS{58} >= CLASS{12});
  CHECK(CLASS{58} >= CLASS{58});
  CHECK_FALSE(CLASS{58} >= CLASS{60});
}

template<typename CLASS>
void comp_tests_three_args() {
  CHECK(CLASS{0, 0, 0} == CLASS{0, 0, 0});
  CHECK(CLASS{58, 3, 2} == CLASS{58, 3, 2});
  CHECK_FALSE(CLASS{12, 3, 2} == CLASS{58, 3, 2});
  CHECK_FALSE(CLASS{12, 3, 2} == CLASS{5, 3, 2});
  CHECK_FALSE(CLASS{12, 3, 2} == CLASS{12, 5, 2});
  CHECK_FALSE(CLASS{12, 3, 2} == CLASS{12, 2, 2});
  CHECK_FALSE(CLASS{12, 3, 2} == CLASS{12, 3, 6});
  CHECK_FALSE(CLASS{12, 3, 2} == CLASS{12, 3, 0});

  CHECK_FALSE(CLASS{58, 3, 2} != CLASS{58, 3, 2});
  CHECK(CLASS{12, 3, 2} != CLASS{58, 3, 2});
  CHECK(CLASS{12, 3, 2} != CLASS{5, 3, 2});
  CHECK(CLASS{12, 3, 2} != CLASS{12, 5, 2});
  CHECK(CLASS{12, 3, 2} != CLASS{12, 2, 2});
  CHECK(CLASS{12, 3, 2} != CLASS{12, 3, 6});
  CHECK(CLASS{12, 3, 2} != CLASS{12, 3, 0});

  CHECK_FALSE(CLASS{0, 0, 0} < CLASS{0, 0, 0});
  CHECK_FALSE(CLASS{58, 3, 2} < CLASS{58, 3, 2});
  CHECK(CLASS{12, 3, 2} < CLASS{58, 3, 2});
  CHECK(CLASS{12, 3, 2} < CLASS{12, 5, 2});
  CHECK(CLASS{12, 3, 2} < CLASS{12, 3, 6});
  CHECK_FALSE(CLASS{12, 3, 2} < CLASS{12, 2, 2});
  CHECK_FALSE(CLASS{12, 3, 2} < CLASS{12, 3, 0});
  CHECK_FALSE(CLASS{12, 3, 2} < CLASS{5, 3, 2});
  CHECK_FALSE(CLASS{15, 1, 2} < CLASS{13, 3, 2});
  CHECK_FALSE(CLASS{15, 5, 1} < CLASS{13, 3, 2});

  CHECK_FALSE(CLASS{0, 0, 0} > CLASS{0, 0, 0});
  CHECK_FALSE(CLASS{58, 3, 2} > CLASS{58, 3, 2});
  CHECK_FALSE(CLASS{12, 3, 2} > CLASS{58, 3, 2});
  CHECK_FALSE(CLASS{12, 3, 2} > CLASS{12, 5, 2});
  CHECK_FALSE(CLASS{12, 3, 2} > CLASS{12, 3, 6});
  CHECK(CLASS{12, 3, 2} > CLASS{12, 2, 2});
  CHECK(CLASS{12, 3, 2} > CLASS{12, 3, 0});
  CHECK(CLASS{12, 3, 2} > CLASS{5, 3, 2});

  CHECK(CLASS{0, 0, 0} <= CLASS{0, 0, 0});
  CHECK(CLASS{58, 3, 2} <= CLASS{58, 3, 2});
  CHECK(CLASS{12, 3, 2} <= CLASS{58, 3, 2});
  CHECK(CLASS{12, 3, 2} <= CLASS{12, 5, 2});
  CHECK(CLASS{12, 3, 2} <= CLASS{12, 3, 6});
  CHECK_FALSE(CLASS{12, 3, 2} <= CLASS{12, 2, 2});
  CHECK_FALSE(CLASS{12, 3, 2} <= CLASS{12, 3, 0});
  CHECK_FALSE(CLASS{12, 3, 2} <= CLASS{5, 3, 2});

  CHECK(CLASS{0, 0, 0} >= CLASS{0, 0, 0});
  CHECK(CLASS{58, 3, 2} >= CLASS{58, 3, 2});
  CHECK_FALSE(CLASS{12, 3, 2} >= CLASS{58, 3, 2});
  CHECK_FALSE(CLASS{12, 3, 2} >= CLASS{12, 5, 2});
  CHECK_FALSE(CLASS{12, 3, 2} >= CLASS{12, 3, 6});
  CHECK(CLASS{12, 3, 2} >= CLASS{12, 2, 2});
  CHECK(CLASS{12, 3, 2} >= CLASS{12, 3, 0});
  CHECK((CLASS{12, 3, 2} >= CLASS{5, 3, 2}));
}


template<typename CLASS>
void eq_tests_two_args() {
  CHECK(CLASS{0, 0} == CLASS{0, 0});
  CHECK(CLASS{58, 3} == CLASS{58, 3});
  CHECK_FALSE(CLASS{12, 3} == CLASS{58, 3});
  CHECK_FALSE(CLASS{12, 3} == CLASS{5, 3});
  CHECK_FALSE(CLASS{12, 3} == CLASS{12, 5});
  CHECK_FALSE(CLASS{12, 3} == CLASS{12, 2});

  CHECK_FALSE(CLASS{58, 3} != CLASS{58, 3});
  CHECK(CLASS{12, 3} != CLASS{58, 3});
  CHECK(CLASS{12, 3} != CLASS{5, 3});
  CHECK(CLASS{12, 3} != CLASS{12, 5});
  CHECK(CLASS{12, 3} != CLASS{12, 2});
}

template<typename CLASS, typename CAST_1, typename CAST_2>
void eq_tests_two_args() {
  CHECK(CLASS{static_cast<CAST_1>(0), static_cast<CAST_2>(0)} == CLASS{static_cast<CAST_1>(0), static_cast<CAST_2>(0)});
  CHECK(CLASS{static_cast<CAST_1>(58), static_cast<CAST_2>(3)} == CLASS{static_cast<CAST_1>(58), static_cast<CAST_2>(3)});
  CHECK_FALSE(CLASS{static_cast<CAST_1>(12), static_cast<CAST_2>(3)} == CLASS{static_cast<CAST_1>(58), static_cast<CAST_2>(3)});
  CHECK_FALSE(CLASS{static_cast<CAST_1>(12), static_cast<CAST_2>(3)} == CLASS{static_cast<CAST_1>(5), static_cast<CAST_2>(3)});
  CHECK_FALSE(CLASS{static_cast<CAST_1>(12), static_cast<CAST_2>(3)} == CLASS{static_cast<CAST_1>(12), static_cast<CAST_2>(5)});
  CHECK_FALSE(CLASS{static_cast<CAST_1>(12), static_cast<CAST_2>(3)} == CLASS{static_cast<CAST_1>(12), static_cast<CAST_2>(2)});

  CHECK_FALSE(CLASS{static_cast<CAST_1>(58), static_cast<CAST_2>(3)} != CLASS{static_cast<CAST_1>(58), static_cast<CAST_2>(3)});
  CHECK(CLASS{static_cast<CAST_1>(12), static_cast<CAST_2>(3)} != CLASS{static_cast<CAST_1>(58), static_cast<CAST_2>(3)});
  CHECK(CLASS{static_cast<CAST_1>(12), static_cast<CAST_2>(3)} != CLASS{static_cast<CAST_1>(5), static_cast<CAST_2>(3)});
  CHECK(CLASS{static_cast<CAST_1>(12), static_cast<CAST_2>(3)} != CLASS{static_cast<CAST_1>(12), static_cast<CAST_2>(5)});
  CHECK(CLASS{static_cast<CAST_1>(12), static_cast<CAST_2>(3)} != CLASS{static_cast<CAST_1>(12), static_cast<CAST_2>(2)});
}


static constexpr auto sampleRdDates = std::array{  // NOLINT(cert-err58-cpp)
                                                 calendars::Date{-214193}, calendars::Date{-61387}, calendars::Date{25469},  calendars::Date{49217},  calendars::Date{171307},
                                                 calendars::Date{210155},  calendars::Date{253427}, calendars::Date{369740}, calendars::Date{400085}, calendars::Date{434355},
                                                 calendars::Date{452605},  calendars::Date{470160}, calendars::Date{473837}, calendars::Date{507850}, calendars::Date{524156},
                                                 calendars::Date{544676},  calendars::Date{567118}, calendars::Date{569477}, calendars::Date{601716}, calendars::Date{613424},
                                                 calendars::Date{626596},  calendars::Date{645554}, calendars::Date{664224}, calendars::Date{671401}, calendars::Date{694799},
                                                 calendars::Date{704424},  calendars::Date{708842}, calendars::Date{709409}, calendars::Date{709580}, calendars::Date{727274},
                                                 calendars::Date{728714},  calendars::Date{744313}, calendars::Date{764652}};
