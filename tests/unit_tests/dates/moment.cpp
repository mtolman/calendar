#include <doctest.h>

#include "src/calendars/moment.h"
#include "common.h"
#include "common_test_headers.h"

TEST_SUITE("Moment") {
  TEST_CASE("Comparisons") {
    comp_test_one_arg<calendars::Moment>();
  }

  TEST_CASE("Conversions") {
    calendars::Moment moment{};
    calendars::Date rdate{};

    rdate.day       = 0;
    moment.datetime = 0.5;
    CHECK_EQ(rdate.day, calendars::Date::from(moment).day);
    CHECK_EQ(rdate.to<calendars::Moment>().datetime, 0);
    CHECK_EQ(calendars::Moment::from(moment).datetime, moment.datetime);
    CHECK_EQ(calendars::Moment::from(moment).to<calendars::Moment>().datetime, moment.datetime);

    rdate.day       = -1;
    moment.datetime = -0.5;
    CHECK_EQ(rdate.day, calendars::Date::from(moment).day);

    moment.datetime = 10.5;
    CHECK_IN_RANGE(0.5, moment.to_time(), 0.01)

    moment.datetime = 12.7895;
    CHECK_IN_RANGE(0.7895, moment.to_time(), 0.01)

    moment.datetime = -12.7895;
    CHECK_IN_RANGE(0.210500, moment.to_time(), 0.01)
  }
}

static_assert(calendars::Moment::epoch == 0);
