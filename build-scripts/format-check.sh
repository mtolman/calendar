#!/usr/bin/env bash
clang-format --dry-run -Werror -style=file unit_tests/* src/*.h src/*.cpp prop_tests/* deps/common_test_headers.h deps/variant_visit.h