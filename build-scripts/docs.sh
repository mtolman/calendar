#!/usr/bin/env bash

rm -rf docs-inter-out
rm -rf docs-out

mkdir -p docs-inter-out
mkdir -p docs-out
cd docs-inter-out || exit 1
cmake .. \
  -DCMAKE_BUILD_TYPE=Release \
  -DBUILD_DOCUMENTATION=On \
  -G Ninja

printf "\n\nGenerating raw doc files...\n\n"
ninja doc-cpp_calendar_lib || exit 1

cd doc/latex || exit 1

printf "\n\nGenerating PDF...\n\n"
make pdf || exit 1
cp refman.pdf ../../../docs-out/calendar-lib.pdf

cd ../

printf "\n\nGenerating HTML Zip...\n\n"
zip -r calendar-html.zip html/*
cp calendar-html.zip ../../docs-out/

printf "\n\nGenerating Man...\n\n"
zip -r calendar-man.zip man/man3/*
cp calendar-man.zip ../../docs-out/

cd ../../docs-out/ || exit 1

