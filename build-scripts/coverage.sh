#!/usr/bin/env bash

THREADS=$(grep ^cpu\\scores /proc/cpuinfo | uniq |  awk '{print $4}')

mkdir -p lcov-out
rm -rf lcov-out/CMakeCache.txt

cd lcov-out || exit 1
cmake .. \
  -DCMAKE_BUILD_TYPE=Debug \
  -DCODE_COVERAGE=ON \
  -G "Unix Makefiles"

make -j $THREADS || exit 1
#make test

function coverage {
  EXE=$1
  make "ccov-$EXE" > "$EXE-cov.log"

  if [ $? -ne 0 ]; then
    printf "Error running coverage for $EXE!\n"
    exit 1;
  fi

  LINE_COV=$(cat "$EXE-cov.log" | grep -A 2 "Overall coverage rate" | grep "%" | awk '{print $2}' | sed -re 's/\.[0-9]+//g' | sed 's/\%//' | head -n 1)
  FN_COV=$(cat "$EXE-cov.log" | grep -A 2 "Overall coverage rate" | grep "%" | awk '{print $2}' | sed -re 's/\.[0-9]+//g' | sed 's/\%//' | tail -n +2)

  printf "Line Coverage for %s: %s\n" "$EXE" "$LINE_COV"
  printf "Function Coverage for %s: %s\n" "$EXE" "$FN_COV"

  if [[ "$LINE_COV" -lt "90" ]]; then
    printf '%s\n' "$EXE has $LINE_COV% coverage! Less than 90% threshold"
    exit 5;
  fi
#  if [[ "$FN_COV" -lt "70" ]]; then
#    printf '%s\n' "$EXE has $FN_COV% coverage! Less than 90% threshold"
#    exit 5;
#  fi
}

coverage calendar_tests
coverage calendar_property_tests
coverage c_calendar_bindings_tests
