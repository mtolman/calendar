#!/usr/bin/env bash

THREADS=$(grep ^cpu\\scores /proc/cpuinfo | uniq |  awk '{print $4}')
THREADS=$(($THREADS * 2))

function sanitize_release() {
    TYPE=$1
    DIR="$TYPE-out-release"

    printf "\n\n\nRunning $TYPE sanitization...\n\n\n"

    mkdir -p $DIR
    rm -rf $DIR/CMakeCache.txt
    cd $DIR || exit 1
    eval "cmake .. -DCMAKE_BUILD_TYPE=Release -DUSE_SANITIZER=$TYPE"

    make calendar_tests -j $THREADS
    make calendar_property_tests -j $THREADS
    make calendar_combinatorial_tests -j $THREADS
    make test
    if [ $? -ne 0 ]; then
      printf "\n\n\nFailed sanitization $TYPE!\n"
      exit 1;
    fi

    printf "\n\n\nPassed sanitization $TYPE!\n"

    cd ..
}

function sanitize_debug() {
    TYPE=$1
    DIR="$TYPE-out-debug"

    printf "\n\n\nRunning $TYPE sanitization...\n\n\n"

    mkdir -p $DIR
    rm -rf $DIR/CMakeCache.txt
    cd $DIR || exit 1
    eval "cmake .. -DCMAKE_BUILD_TYPE=Debug -DUSE_SANITIZER=$TYPE"

    make calendar_tests -j $THREADS
    make calendar_property_tests -j $THREADS
    make calendar_combinatorial_tests -j $THREADS
    make test
    if [ $? -ne 0 ]; then
      printf "\n\n\nFailed sanitization $TYPE!\n"
      exit 1;
    fi

    printf "\n\n\nPassed sanitization $TYPE!\n"

    cd ..
}

function sanitize() {
  sanitize_debug $1
  sanitize_release $1
}

sanitize Address
sanitize Thread
sanitize Leak