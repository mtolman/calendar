#!/usr/bin/env bash

mkdir -p deps-out
rm -rf deps-out/CMakeCache.txt
cd deps-out || exit 1
cmake .. \
  -DCMAKE_BUILD_TYPE=Debug \
  -G Ninja

cp $(ninja dep-graph-src | grep "Dependency graph for" | awk '{print $NF}') ../
