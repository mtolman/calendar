#!/usr/bin/env bash
rm -rf out/
rm -rf *-out/
rm -rf *-out-debug/
rm -rf *-out-release/
rm -f version.cmake
rm -f src/version.h