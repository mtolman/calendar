#!/usr/bin/env bash

printf "\n\nCleaning...\n\n"
bash clean.sh || exit 1
printf "\n\nCollecting Version Information...\n\n"
bash setup-version-info.sh || exit 1
printf "\n\nChecking code format...\n\n"
bash ./build-scripts/format-check.sh || exit 1
printf "\n\nLinting...\n\n"
bash ./build-scripts/clang-tidy.sh || exit 1
printf "\n\nSpell Checking...\n\n"
bash ./build-scripts/spellcheck.sh || exit 1

printf "\n\nBuilding...\n\n"
mkdir bin-out
cd bin-out || exit 1
cmake -G Ninja -DCMAKE_BUILD_TYPE=Release .. || exit 1
ninja || exit 1
ninja test
