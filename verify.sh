#!/usr/bin/env bash

printf "\n\nRunning debug test build...\n\n"
bash ./build-scripts/ctest.sh || exit 1
printf "\n\nGathering Coverage...\n\n"
bash ./build-scripts/coverage.sh || exit 1
printf "\n\nLooking for leaks and defects...\n\n"
bash ./build-scripts/sanitize.sh || exit 1
#bash ./build-scripts/dependencies.sh

# TODO: uncomment
#bash ./build-scripts/upload.sh
