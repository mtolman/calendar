#pragma once

#include "impl.h"
#include "../calendars/hebrew_date.h"

/**
   * Holidays on the Hebrew calendars
 */
namespace calendars::holidays::hebrew {
  CAL_CONSTEXPR_FN Date yom_kippur(int64_t hebrew_year) { return Date{HebrewDate{hebrew_year, HebrewDate::MONTH::TISHRI, 10}}; }

  CAL_CONSTEXPR_FN Date rosh_ha_shanah(int64_t hebrew_year) { return Date{HebrewDate{hebrew_year, HebrewDate::MONTH::TISHRI, 1}}; }

  CAL_CONSTEXPR_FN Date sukkot(int64_t hebrew_year) { return Date{HebrewDate{hebrew_year, HebrewDate::MONTH::TISHRI, 15}}; }

  CAL_CONSTEXPR_FN Date hoshana_rabba(int64_t hebrew_year) { return Date{HebrewDate{hebrew_year, HebrewDate::MONTH::TISHRI, 21}}; }

  CAL_CONSTEXPR_FN Date shemini_azeret(int64_t hebrew_year) { return Date{HebrewDate{hebrew_year, HebrewDate::MONTH::TISHRI, 22}}; }

  CAL_CONSTEXPR_FN Date simhat_torah(int64_t hebrew_year) { return Date{HebrewDate{hebrew_year, HebrewDate::MONTH::TISHRI, 23}}; }

  CAL_CONSTEXPR_FN Date passover(int64_t hebrew_year) { return Date{HebrewDate{hebrew_year, HebrewDate::MONTH::NISAN, 15}}; }

  CAL_CONSTEXPR_FN Date passover_end(int64_t hebrew_year) { return Date{HebrewDate{hebrew_year, HebrewDate::MONTH::NISAN, 21}}; }

  CAL_CONSTEXPR_FN Date shavout(int64_t hebrew_year) { return Date{HebrewDate{hebrew_year, HebrewDate::MONTH::SIVAN, 6}}; }

  CAL_CONSTEXPR_FN std::array<Date, 6> sukkot_intermediate_days(int64_t hebrew_year) {
    return {Date{HebrewDate{hebrew_year, HebrewDate::MONTH::TISHRI, 16}},
            Date{HebrewDate{hebrew_year, HebrewDate::MONTH::TISHRI, 17}},
            Date{HebrewDate{hebrew_year, HebrewDate::MONTH::TISHRI, 18}},
            Date{HebrewDate{hebrew_year, HebrewDate::MONTH::TISHRI, 19}},
            Date{HebrewDate{hebrew_year, HebrewDate::MONTH::TISHRI, 20}},
            Date{HebrewDate{hebrew_year, HebrewDate::MONTH::TISHRI, 21}}};
  }

  CAL_CONSTEXPR_FN std::array<Date, 5> passover_days(int64_t hebrew_year) {
    return {Date{HebrewDate{hebrew_year, HebrewDate::MONTH::NISAN, 16}},
            Date{HebrewDate{hebrew_year, HebrewDate::MONTH::NISAN, 17}},
            Date{HebrewDate{hebrew_year, HebrewDate::MONTH::NISAN, 18}},
            Date{HebrewDate{hebrew_year, HebrewDate::MONTH::NISAN, 19}},
            Date{HebrewDate{hebrew_year, HebrewDate::MONTH::NISAN, 20}}};
  }

  CAL_CONSTEXPR_FN std::array<Date, 8> hanukkah(int64_t hebrew_year) {
    return {
        Date{HebrewDate{hebrew_year, HebrewDate::MONTH::KISLEV, 25}},
        Date{HebrewDate{hebrew_year, HebrewDate::MONTH::KISLEV, 25}}.add_days(1),
        Date{HebrewDate{hebrew_year, HebrewDate::MONTH::KISLEV, 25}}.add_days(2),
        Date{HebrewDate{hebrew_year, HebrewDate::MONTH::KISLEV, 25}}.add_days(3),
        Date{HebrewDate{hebrew_year, HebrewDate::MONTH::KISLEV, 25}}.add_days(4),
        Date{HebrewDate{hebrew_year, HebrewDate::MONTH::KISLEV, 25}}.add_days(5),
        Date{HebrewDate{hebrew_year, HebrewDate::MONTH::KISLEV, 25}}.add_days(6),
        Date{HebrewDate{hebrew_year, HebrewDate::MONTH::KISLEV, 25}}.add_days(7),
    };
  }

  CAL_CONSTEXPR_FN Date tu_b_shevat(int64_t hebrew_year) { return Date{HebrewDate{hebrew_year, HebrewDate::MONTH::SHEVAT, 15}}; }

  CAL_CONSTEXPR_FN Date purim(int64_t hebrew_year) { return Date{HebrewDate{hebrew_year, HebrewDate::last_month_of_hebrew_year(hebrew_year), 14}}; }

  CAL_CONSTEXPR_FN Date ta_anit_esther(int64_t hebrew_year) {
    const auto purimDate = purim(hebrew_year);
    if (purimDate.day_of_week() == DAY_OF_WEEK::SUNDAY) {
      return purimDate.sub_days(3);
    }
    else {
      return purimDate.sub_days(1);
    }
  }

  CAL_CONSTEXPR_FN Date postpone_if_saturday(const Date& date) {
    if (date.day_of_week() == DAY_OF_WEEK::SATURDAY) {
      return date.add_days(1);
    }
    return date;
  }

  CAL_CONSTEXPR_FN Date tishah_be_av(int64_t hebrew_year) { return postpone_if_saturday(Date{HebrewDate{hebrew_year, HebrewDate::MONTH::AV, 9}}); }

  CAL_CONSTEXPR_FN Date tzom_gedaliah(int64_t hebrew_year) { return postpone_if_saturday(Date{HebrewDate{hebrew_year, HebrewDate::MONTH::TISHRI, 3}}); }

  CAL_CONSTEXPR_FN Date tzom_tammuz(int64_t hebrew_year) { return postpone_if_saturday(Date{HebrewDate{hebrew_year, HebrewDate::MONTH::TAMMUZ, 17}}); }

  CAL_CONSTEXPR_FN Date yom_ha_shoah(int64_t hebrew_year) { return postpone_if_saturday(Date{HebrewDate{hebrew_year, HebrewDate::MONTH::NISAN, 27}}); }

  CAL_CONSTEXPR_FN Date yom_ha_zikkaron(int64_t hebrew_year) {
    const auto iyyar4    = Date{HebrewDate{hebrew_year, HebrewDate::MONTH::IYYAR, 4}};
    const auto dayOfWeek = iyyar4.day_of_week();
    if (dayOfWeek == DAY_OF_WEEK::THURSDAY || dayOfWeek == DAY_OF_WEEK::FRIDAY) {
      return iyyar4.day_of_week_before(DAY_OF_WEEK::WEDNESDAY);
    }
    else if (dayOfWeek == DAY_OF_WEEK::SUNDAY) {
      return iyyar4.add_days(1);
    }
    else {
      return iyyar4;
    }
  }

  CAL_CONSTEXPR_FN std::optional<int64_t> omer(const Date& date) {
    const auto c = date.day_difference(hebrew::passover(date.to<HebrewDate>().year));
    if (1 <= c && c <= 49) {
      return math::mod(math::floor<double, int64_t>(static_cast<double>(c) / 7.0), 7);
    }
    return std::nullopt;
  }

  CAL_CONSTEXPR_FN Date birthday(const Date& birthDate, int64_t hebrewYear) {
    const auto hBirthDate = birthDate.to<HebrewDate>();
    const auto lastMonth  = HebrewDate::last_month_of_hebrew_year(hebrewYear);
    if (hBirthDate.month == lastMonth) {
      return Date{HebrewDate{hebrewYear, lastMonth, hBirthDate.day}};
    }
    else {
      return Date{HebrewDate{hebrewYear, hBirthDate.month, 1}}.add_days(hBirthDate.day - 1);
    }
  }

  CAL_CONSTEXPR_FN Date yahrzeit(const Date& deathDate, int64_t hebrewYear) {
    const auto hDeathDate = deathDate.to<HebrewDate>();
    if (hDeathDate.month == HebrewDate::MONTH::MARHESHVAN && hDeathDate.day == 30 && !HebrewDate::long_marheshvan(hebrewYear)) {
      return Date{HebrewDate{hebrewYear, HebrewDate::MONTH::KISLEV, 1}}.sub_days(1);
    }
    else if (hDeathDate.month == HebrewDate::MONTH::KISLEV && hDeathDate.day == 30 && HebrewDate::short_kislev(hebrewYear)) {
      return Date{HebrewDate{hebrewYear, HebrewDate::MONTH::TEVET, 1}}.sub_days(1);
    }
    else if (hDeathDate.month == HebrewDate::MONTH::ADAR_II) {
      return Date{HebrewDate{hebrewYear, HebrewDate::last_month_of_hebrew_year(hebrewYear), hDeathDate.day}};
    }
    else if (hDeathDate.month == HebrewDate::MONTH::ADAR && hDeathDate.day == 30 && !HebrewDate::is_leap_year(hebrewYear)) {
      return Date{HebrewDate{hebrewYear, HebrewDate::MONTH::SHEVAT, 30}};
    }
    else {
      return Date{HebrewDate{hebrewYear, hDeathDate.month, 1}}.add_days(hDeathDate.day - 1);
    }
  }  // namespace hebrew

  namespace gregorian {
#define CAL_HEBREW_SINGLE_HOLIDAY(holiday)                                                                                                           \
  inline std::vector<Date> holiday(int64_t gregorian_year) {                                                                                         \
    CAL_CONSTEXPR_VAR auto hd = calendars::holidays::hebrew::holiday(2021).to<HebrewDate>();                                                          \
    return impl::three_date_in_gregorian<HebrewDate>(hd.month, hd.day, gregorian_year);                                                              \
  }

#define CAL_HEBREW_MULT_HOLIDAYS(holiday)                                                                                                            \
  inline std::vector<Date> holiday(int64_t gregorian_year) {                                                                                         \
    CAL_CONSTEXPR_VAR auto hds = calendars::holidays::hebrew::holiday(2021);                                                                          \
    std::vector<Date>      res;                                                                                                                      \
    res.reserve(hds.size());                                                                                                                         \
    for (const auto& d : hds) {                                                                                                                      \
      const auto hd  = d.to<HebrewDate>();                                                                                                           \
      const auto gds = impl::three_date_in_gregorian<HebrewDate>(hd.month, hd.day, gregorian_year);                                                  \
      for (decltype(auto) gd : gds) {                                                                                                                \
        res.emplace_back(gd);                                                                                                                        \
      }                                                                                                                                              \
    }                                                                                                                                                \
    return res;                                                                                                                                      \
  }

    CAL_HEBREW_SINGLE_HOLIDAY(yom_kippur)
    CAL_HEBREW_SINGLE_HOLIDAY(rosh_ha_shanah)
    CAL_HEBREW_SINGLE_HOLIDAY(sukkot)
    CAL_HEBREW_SINGLE_HOLIDAY(hoshana_rabba)
    CAL_HEBREW_SINGLE_HOLIDAY(shemini_azeret)
    CAL_HEBREW_SINGLE_HOLIDAY(simhat_torah)
    CAL_HEBREW_SINGLE_HOLIDAY(tzom_gedaliah)
    CAL_HEBREW_SINGLE_HOLIDAY(passover)
    CAL_HEBREW_SINGLE_HOLIDAY(passover_end)
    CAL_HEBREW_SINGLE_HOLIDAY(shavout)
    CAL_HEBREW_SINGLE_HOLIDAY(tu_b_shevat)
    CAL_HEBREW_SINGLE_HOLIDAY(purim)
    CAL_HEBREW_SINGLE_HOLIDAY(ta_anit_esther)
    CAL_HEBREW_SINGLE_HOLIDAY(tishah_be_av)
    CAL_HEBREW_SINGLE_HOLIDAY(tzom_tammuz)
    CAL_HEBREW_SINGLE_HOLIDAY(yom_ha_shoah)

    CAL_HEBREW_MULT_HOLIDAYS(sukkot_intermediate_days)
    CAL_HEBREW_MULT_HOLIDAYS(hanukkah)
    CAL_HEBREW_MULT_HOLIDAYS(passover_days)

    inline std::vector<Date> birthday(const Date& birthDate, int64_t gregorian_year) {
      const auto hd = calendars::holidays::hebrew::birthday(birthDate, 2021).to<HebrewDate>();
      return impl::three_date_in_gregorian<HebrewDate>(hd.month, hd.day, gregorian_year);
    }

    inline std::vector<Date> yahrzeit(const Date& deathDate, int64_t gregorian_year) {
      const auto hd = calendars::holidays::hebrew::yahrzeit(deathDate, 2021).to<HebrewDate>();
      return impl::three_date_in_gregorian<HebrewDate>(hd.month, hd.day, gregorian_year);
    }

#undef CAL_HEBREW_SINGLE_HOLIDAY
#undef CAL_HEBREW_MULT_HOLIDAYS

    std::vector<Date> sh_ela(int64_t gregorian_year);

    std::vector<Date> birkath_ha_hama(int64_t gregorian_year);
  }  // namespace gregorian
}    // namespace hebrew
