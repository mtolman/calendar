#pragma once

#include "impl.h"

/**
 * Collection of holiday calculation functions
 */
namespace calendars::holidays::usa {
  CAL_CONSTEXPR_FN Date independence_day(int64_t year) { return Date{GregorianDate{year, GregorianDate::MONTH::JULY, 4}}; }

  CAL_CONSTEXPR_FN Date labor_day(int64_t year) {
    return impl::first_week_day(DAY_OF_WEEK::MONDAY, Date{GregorianDate{year, GregorianDate::MONTH::SEPTEMBER, 1}});
  }

  CAL_CONSTEXPR_FN Date memorial_day(int64_t year) {
    return impl::last_week_day(DAY_OF_WEEK::MONDAY, Date{GregorianDate{year, GregorianDate::MONTH::MAY, 31}});
  }

  CAL_CONSTEXPR_FN Date election_day(int64_t year) {
    return impl::first_week_day(DAY_OF_WEEK::TUESDAY, Date{GregorianDate{year, GregorianDate::MONTH::NOVEMBER, 2}});
  }
}  // namespace usa