#include "misc.h"

std::vector<calendars::Date> calendars::holidays::misc::unlucky_fridays_in_range(std::tuple<Date, Date> range) {
  std::vector<Date> res{};
  res.reserve(std::get<1>(range).day_difference(std::get<0>(range)) / 30 + 1);
  auto cur = std::get<0>(range).day_of_week_on_or_after(DAY_OF_WEEK::FRIDAY);
  while (cur <= std::get<1>(range)) {
    if (cur.to<GregorianDate>().day == 13) {
      res.emplace_back(cur);
    }
    cur = cur.add_days(7);
  }
  res.shrink_to_fit();
  return res;
}

std::vector<calendars::Date> calendars::holidays::misc::unlucky_fridays_in_year(int64_t year) {
  return unlucky_fridays_in_range(calendars::GregorianDate::year_boundaries(year));
}
