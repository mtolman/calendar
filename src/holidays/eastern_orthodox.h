#pragma once

#include "impl.h"

/**
   * Holidays associated with Eastern Orthodox churches that are calculated using the Julian calendars.
 */
namespace calendars::holidays::eastern_orthodox {
  namespace julian {
    CAL_CONSTEXPR_FN Date christmas_day(int64_t julianYear) { return Date{JulianDate{julianYear, JulianDate::MONTH::DECEMBER, 25}}; }

    CAL_CONSTEXPR_FN Date nativity_of_the_virgin_mary(int64_t julianYear) { return Date{JulianDate{julianYear, JulianDate::MONTH::SEPTEMBER, 8}}; }

    CAL_CONSTEXPR_FN Date elevation_of_the_life_giving_cross(int64_t julianYear) { return Date{JulianDate{julianYear, JulianDate::MONTH::SEPTEMBER, 14}}; }

    CAL_CONSTEXPR_FN Date presentation_of_the_virgin_mary_in_the_temple(int64_t julianYear) {
      return Date{JulianDate{julianYear, JulianDate::MONTH::NOVEMBER, 21}};
    }

    CAL_CONSTEXPR_FN Date theophany(int64_t julianYear) { return Date{JulianDate{julianYear, JulianDate::MONTH::JANUARY, 6}}; }

    CAL_CONSTEXPR_FN Date presentation_of_christ_in_the_temple(int64_t julianYear) { return Date{JulianDate{julianYear, JulianDate::MONTH::FEBRUARY, 2}}; }

    CAL_CONSTEXPR_FN Date the_annunciation(int64_t julianYear) { return Date{JulianDate{julianYear, JulianDate::MONTH::MARCH, 25}}; }

    CAL_CONSTEXPR_FN Date the_transfiguration(int64_t julianYear) { return Date{JulianDate{julianYear, JulianDate::MONTH::AUGUST, 6}}; }

    CAL_CONSTEXPR_FN Date the_repose_of_the_virgin_mary(int64_t julianYear) { return Date{JulianDate{julianYear, JulianDate::MONTH::AUGUST, 15}}; }

    CAL_CONSTEXPR_FN std::array<Date, 14> the_fast_of_the_repose_of_the_virgin_mary(int64_t julianYear) {
      return impl::day_sequence<JulianDate>(Date{JulianDate{julianYear, JulianDate::MONTH::AUGUST, 1}}, std::make_index_sequence<14>());
    }

    CAL_CONSTEXPR_FN std::array<Date, 40> the_40_day_christmas_fast(int64_t julianYear) {
      return impl::day_sequence<JulianDate>(Date{JulianDate{julianYear, JulianDate::MONTH::NOVEMBER, 15}}, std::make_index_sequence<40>());
    }

    CAL_CONSTEXPR_FN Date easter(int64_t julianYear) {
      // For math to work out in BC years, we shift up by 1 so that 1 BC becomes 0
      //  This is important since 0 is not a valid julian year
      const auto adjustedYear = julianYear >= 0 ? julianYear : julianYear + 1;
      const auto shiftedEpact = math::mod(14 + 11 * math::mod(adjustedYear, 19), 30);
      const auto paschalMoon  = Date{JulianDate{julianYear, JulianDate::MONTH::APRIL, 19}}.sub_days(shiftedEpact);
      return paschalMoon.day_of_week_after(DAY_OF_WEEK::SUNDAY);
    }
  }  // namespace julian


  namespace gregorian {
    std::vector<Date> christmas_day(int64_t gregorianYear);

    std::vector<Date> nativity_of_the_virgin_mary(int64_t gregorianYear);

    std::vector<Date> elevation_of_the_life_giving_cross(int64_t gregorianYear);

    std::vector<Date> presentation_of_the_virgin_mary_in_the_temple(int64_t gregorianYear);

    std::vector<Date> theophany(int64_t gregorianYear);

    std::vector<Date> presentation_of_christ_in_the_temple(int64_t gregorianYear);

    std::vector<Date> the_annunciation(int64_t gregorianYear);

    std::vector<Date> the_transfiguration(int64_t gregorianYear);

    std::vector<Date> the_repose_of_the_virgin_mary(int64_t gregorianYear);

    std::vector<Date> the_fast_of_the_repose_of_the_virgin_mary(int64_t gregorianYear);

    std::vector<Date> the_40_day_christmas_fast(int64_t gregorianYear);

    std::vector<Date> easter(int64_t gregorianYear);
  }  // namespace gregorian
}    // namespace eastern_orthodox
