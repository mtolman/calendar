#pragma once

#include <string>

namespace calendars::str {
  inline std::string from_u8string(const std::string &s) { return s; }
  inline std::string from_u8string(std::string &&s) { return std::move(s); }
#ifdef __cpp_lib_char8_t
  inline std::string from_u8string(const std::u8string &s) { return std::string(s.begin(), s.end()); }
#endif
}  // namespace calendars::str
