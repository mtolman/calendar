#pragma once

#include "common.h"

namespace calendars {
  /**
   * Date on the Icelandic Calendar
   */
  struct IcelandicDate {
    /**
     * Represents seasons in degrees
     */
    enum class SEASON : int16_t {
      SPRING = 0,
      SUMMER = 90,
      AUTUMN = 180,
      WINTER = 270
    };
    enum class MONTH {
      HARPA,
      SKERPLA,
      SOLMANUDUR,
      HEYANNIR,
      TVIMANUDUR,
      HAUSTMANUDUR,
      GORMANUDUR,
      YLIR,
      MORSUGUR,
      THORRI,
      GOA,
      EINMANUDUR,
    };

    static constexpr int64_t epoch = 109;
    int64_t                  year;
    SEASON                   season;
    int8_t                   week;
    DAY_OF_WEEK              weekday;

    CAL_CONSTEXPR_CLASS_FN IcelandicDate() noexcept = default;
    CAL_CONSTEXPR_CLASS_FN IcelandicDate(int64_t year, SEASON season, int8_t week, DAY_OF_WEEK weekday) noexcept
        : year(year), season(season), week(week), weekday(weekday) {}
    CAL_CONSTEXPR_CLASS_FN IcelandicDate(int64_t year, int16_t season, int8_t week, int8_t weekday) noexcept
        : IcelandicDate(year, static_cast<SEASON>(season), week, static_cast<DAY_OF_WEEK>(math::abs(math::mod(weekday, 6)))) {}

    [[nodiscard]] static CAL_CONSTEXPR_FN Date summer(int64_t year) {
      using YxA        = math::YxA<math::Radix<4, 25, 4>, math::Radix<97, 24, 1, 0>>;
      const auto apr19 = Date{epoch + 365 * (year - 1) + YxA::sum(year)};
      return apr19.day_of_week_on_or_after(DAY_OF_WEEK::THURSDAY);
    }
    [[nodiscard]] static CAL_CONSTEXPR_FN Date winter(int64_t year) { return summer(year + 1).sub_days(180); }

    [[nodiscard]] static CAL_CONSTEXPR_FN bool is_leap_year(int64_t year) { return summer(year + 1).day_difference(winter(year)) != 364; }

    [[nodiscard]] CAL_CONSTEXPR_FN MONTH month() const {
      const auto midsummer = IcelandicDate::winter(year).sub_days(90);
      const auto date      = Date::from(*this);
      const auto start     = season == SEASON::WINTER
                                 ? IcelandicDate::winter(year)
                                 : (date >= midsummer ? midsummer.sub_days(90) : (date < summer(year).add_days(90) ? summer(year) : midsummer));
      return static_cast<MONTH>(math::floor<double, int>(static_cast<double>(date.day_difference(start)) / 30.0) + 1);
    }

    CAL_DEF_OPS_CONSTEXPR(IcelandicDate)

    [[nodiscard]] CAL_CONSTEXPR_FN bool operator<(const IcelandicDate& o) const noexcept {
      return chain_lt(year, o.year, season, o.season, week, o.week, weekday, o.weekday);
    }

    [[nodiscard]] static CAL_CONSTEXPR_FN IcelandicDate from_date(const Date& date) noexcept {
      const auto approx       = math::floor<double, int64_t>((static_cast<double>(date.day - IcelandicDate::epoch + 369) * 400.0) / 146097.0);
      const auto approxSummer = IcelandicDate::summer(approx);
      const auto year         = date >= approxSummer ? approx : approx - 1;
      const auto winter       = IcelandicDate::winter(year);
      const auto season       = date < winter ? SEASON::SUMMER : SEASON::WINTER;
      const auto start        = season == SEASON::SUMMER ? IcelandicDate::summer(year) : IcelandicDate::winter(year);
      const auto week         = math::floor<double, int64_t>(static_cast<double>(date.day_difference(start)) / 7.0) + 1;
      const auto weekday      = date.day_of_week();
      return IcelandicDate{year, season, static_cast<int8_t>(week), weekday};
    }

    [[nodiscard]] CAL_CONSTEXPR_FN Date to_date() const noexcept {
      const auto start = season == SEASON::SUMMER ? IcelandicDate::summer(year) : IcelandicDate::winter(year);
      const auto shift = season == SEASON::SUMMER ? DAY_OF_WEEK::THURSDAY : DAY_OF_WEEK::SATURDAY;
      
      return start.template add_days<>(7LL * (week - 1) + math::mod(static_cast<int64_t>(weekday) - static_cast<int64_t>(shift), 7));
    }
  };
}  // namespace calendars