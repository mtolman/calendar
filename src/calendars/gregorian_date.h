#pragma once

#include <tuple>
#include <array>
#include <vector>
#include "common.h"

namespace calendars {
  /**
   * Represents a date on the Gregorian calendars
   */
  struct GregorianDate {
    static constexpr int64_t epoch = 1;

    /**
     * Year for the Gregorian Date
     * Positive years are C.E
     * Negative years are B.C.E.
     */
    int64_t year = 0;

    enum class MONTH : int16_t {
      JANUARY   = 1,
      FEBRUARY  = 2,
      MARCH     = 3,
      APRIL     = 4,
      MAY       = 5,
      JUNE      = 6,
      JULY      = 7,
      AUGUST    = 8,
      SEPTEMBER = 9,
      OCTOBER   = 10,
      NOVEMBER  = 11,
      DECEMBER  = 12
    };

    /**
     * MONTH of the date
     * Between [1,12]
     */
    MONTH month = MONTH::JANUARY;

    /**
     * Day of the month for the date
     * Between [1,31]
     */
    int16_t day = 1;

    CAL_CONSTEXPR_CLASS_FN GregorianDate() noexcept = default;
    CAL_CONSTEXPR_CLASS_FN GregorianDate(int64_t year, MONTH month, int16_t day) noexcept : year(year), month(month), day(day) {}
    CAL_CONSTEXPR_CLASS_FN GregorianDate(int64_t year, int16_t month, int16_t day) noexcept
        : GregorianDate(year, static_cast<MONTH>(math::abs(math::amod(month, 12))), math::abs(day)) {}

    [[nodiscard]] CAL_CONSTEXPR_FN bool operator<(const GregorianDate& o) const noexcept {
      return chain_lt(year, o.year, month, o.month, day, o.day);
    }

    CAL_DEF_OPS_CONSTEXPR(GregorianDate)

    [[nodiscard]] static CAL_CONSTEXPR_FN bool is_leap_year(int64_t year) {
      const auto yearMod400 = math::mod(year, 400);
      return math::mod(year, 4) == 0 && yearMod400 != 100 && yearMod400 != 200 && yearMod400 != 300;
    }

    [[nodiscard]] static CAL_CONSTEXPR_FN Date year_start(int64_t year) noexcept {
      return Date::from(GregorianDate{year, MONTH::JANUARY, 1});
    }

    [[nodiscard]] static CAL_CONSTEXPR_FN Date year_end(int64_t year) noexcept {
      return Date::from(GregorianDate{year, MONTH::DECEMBER, 31});
    }
    static std::vector<Date>                   year_vector(int64_t year) noexcept;

    template<int64_t Year>
    [[nodiscard]] static CAL_CONSTEXPR_FN decltype(auto) year_range_constexpr() noexcept {
#if CAL_USE_CONSTEXPR
      CAL_CONSTEXPR_VAR auto numDays = GregorianDate::is_leap_year(Year) ? 366 : 365;
      return impl::n_dates_starting_at(GregorianDate::year_start(Year), std::make_index_sequence<numDays>());
#else
      const auto numDays = GregorianDate::is_leap_year(Year) ? 366 : 365;
      return impl::n_dates_starting_at(GregorianDate::year_start(Year), numDays);
#endif
    }

    [[nodiscard]] static CAL_CONSTEXPR_FN std::tuple<Date, Date> year_boundaries(int64_t year) noexcept {
      return std::make_tuple(year_start(year), year_end(year));
    }

    [[nodiscard]] CAL_CONSTEXPR_FN decltype(auto) day_number() const noexcept { return day_difference(GregorianDate{year - 1, MONTH::DECEMBER, 31}); }

    [[nodiscard]] CAL_CONSTEXPR_FN decltype(auto) days_remaining() const noexcept {
      return GregorianDate{year, MONTH::DECEMBER, 31}.day_difference(*this);
    }

    [[nodiscard]] static CAL_CONSTEXPR_FN int64_t year_from_date(const Date& d) noexcept {
      const auto d0 = d.day - GregorianDate::epoch;
      // 146097 = number of days in 400 years (adjusted for leap years)
      const auto n400 = math::floor<double, int64_t>(static_cast<double>(d0) / 146097.0);
      const auto d1   = math::mod(d0, 146097);
      // 36524 = number of days in 100 years (adjusted for leap years)
      const auto n100 = math::floor<double, int64_t>(static_cast<double>(d1) / 36524.0);
      const auto d2   = math::mod(d1, 36524);
      // 1461 = number of days in 4 years (adjusted for leap years)
      const auto n4 = math::floor<double, int64_t>(static_cast<double>(d2) / 1461.0);
      const auto d3 = math::mod(d2, 1461);
      const auto n1 = math::floor<double, int64_t>(static_cast<double>(d3) / 365.0);
      const auto year =
          400 * n400 + 100 * n100 + 4 * n4 + n1 + 1 - static_cast<int64_t>(static_cast<unsigned>(n100 == 4) | static_cast<unsigned>(n1 == 4));
      return year;
    }

    [[nodiscard]] CAL_CONSTEXPR_FN int64_t day_difference(const GregorianDate& other) const noexcept {
      return Date::from(*this).day_difference(Date::from(other));
    }

    [[nodiscard]] static CAL_CONSTEXPR_FN GregorianDate from_date(const Date& rdDate) noexcept {
      const auto year       = GregorianDate::year_from_date(rdDate);
      const auto priorDays  = rdDate.day_difference(GregorianDate::year_start(year));
      const auto marchFirst = GregorianDate{year, GregorianDate::MONTH::MARCH, 1};
      const auto correction = rdDate < Date::from(marchFirst) ? 0 : GregorianDate::is_leap_year(year) ? 1 : 2;
      const auto month      = math::floor<double, int16_t>(static_cast<double>(12 * (priorDays + correction) + 373) / 367.0);
      return GregorianDate{year, month, static_cast<int16_t>(rdDate.day_difference(Date::from(GregorianDate{year, month, 1})) + 1)};
    }

    [[nodiscard]] CAL_CONSTEXPR_FN Date to_date() const noexcept {
      return Date{GregorianDate::epoch - 1 + 365 * (year - 1) + math::floor<double, int64_t>(static_cast<double>(year - 1) / 4.0)
                    - math::floor<double, int64_t>(static_cast<double>(year - 1) / 100.0) + math::floor<double, int64_t>(static_cast<double>(year - 1) / 400.0)
                    + math::floor<double, int64_t>((367 * static_cast<double>(month) - 362) / 12.0)
                    + (static_cast<int16_t>(month) <= 2 ? 0
                       : GregorianDate::is_leap_year(year) ? -1
                                                             : -2)
                    + day};
    }

   private:
    static constexpr std::array<int64_t, 3> leapYearExclusions = {100, 200, 300};
  };
}
