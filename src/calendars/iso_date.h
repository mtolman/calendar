#pragma once

#include "gregorian_date.h"

namespace calendars {
  /**
   * Represents a date on the ISO calendars
   */
  struct IsoDate {
    int64_t year;
    int16_t week;
    int16_t day;


    CAL_CONSTEXPR_CLASS_FN IsoDate() noexcept = default;
    CAL_CONSTEXPR_CLASS_FN IsoDate(int64_t year, int16_t week, int16_t day) noexcept
        : year(year), week(week), day(day) {}

    [[nodiscard]] CAL_CONSTEXPR_FN bool operator<(const IsoDate& o) const noexcept {
      return chain_lt(year, o.year, week, o.week, day, o.day);
    }

    CAL_DEF_OPS_CONSTEXPR(IsoDate)

    [[nodiscard]] static CAL_CONSTEXPR_FN bool is_long_year(int64_t year) noexcept {
      const auto [ jan1, dec31 ] = GregorianDate::year_boundaries(year);
      return jan1.day_of_week() == DAY_OF_WEEK::THURSDAY || dec31.day_of_week() == DAY_OF_WEEK::THURSDAY;
    }
    
    [[nodiscard]] static CAL_CONSTEXPR_FN IsoDate from_date(const Date& rdDate) noexcept {
      const auto    approx  = rdDate.sub_days(3).to<GregorianDate>().year;
      const auto    isoYear = (rdDate >= Date::from(IsoDate{approx + 1, 1, 1}) ? approx + 1 : approx);
      const int16_t isoWeek =
          static_cast<int16_t>(math::floor(static_cast<double>(rdDate.day_difference(Date::from(IsoDate{isoYear, 1, 1}))) / 7.0) + 1);
      const int16_t isoDay = static_cast<int16_t>(math::amod(rdDate.day_difference(Date{0}), 7));
      return IsoDate{isoYear, isoWeek, isoDay};
    }
    
    [[nodiscard]] CAL_CONSTEXPR_FN Date to_date() const noexcept {
      const auto gregReference = Date::from(GregorianDate{year - 1, GregorianDate::MONTH::DECEMBER, 28});
      return gregReference.nth_week_day(week, DAY_OF_WEEK::SUNDAY).add_days(day);
    }
  };
}
