#pragma once

#include "old_hindu_solar_date.h"

namespace calendars {
  /**
   * Arithmetical implemantation of old Hindu Luni-Solar calendars that was based on
   *  mean astronomical events rather than true astronomical events.
   *
   * Do note that whil this is a historical calendars, no attempt is made to ensure
   *  historical accuracy with historical records.
   */
  struct OldHinduLuniSolarDate {
    static constexpr auto epoch = OldHinduSolarDate::epoch;
    
    enum class MONTH
    {
      CAITRA,
      VAISAKHA,
      JYESTHA,
      ASADHA,
      SRAVANA,
      BHADRAPADA,
      ASVINA,
      KARTIKA,
      MARGASIRSA,
      PAUSA,
      MAGHA,
      PHALGUNA
    };

    int64_t year;
    MONTH   month;
    bool    isLeapMonth;
    int16_t day;

    CAL_CONSTEXPR_CLASS_FN OldHinduLuniSolarDate() noexcept = default;
    CAL_CONSTEXPR_CLASS_FN OldHinduLuniSolarDate(int64_t year, MONTH month, bool isLeapMonth, int16_t day) noexcept
        : year(year), month(month), isLeapMonth(isLeapMonth), day(day) {}
    CAL_CONSTEXPR_CLASS_FN OldHinduLuniSolarDate(int64_t year, int16_t month, bool isLeapMonth, int16_t day) noexcept
        : OldHinduLuniSolarDate(year, static_cast<MONTH>(math::abs(math::mod(month, 12))), isLeapMonth, math::abs(day)) {}


    static constexpr double aryaLunarMonth = 1577917500.0 / 53433336.0;
    static constexpr double aryaLunarDay   = aryaLunarMonth / 30.0;

    [[nodiscard]] static CAL_CONSTEXPR_FN bool is_leap_year(int64_t year) {
      return math::mod(static_cast<double>(year) * OldHinduSolarDate::aryaSolarYear - OldHinduSolarDate::aryaSolarMonth, aryaLunarMonth)
             >= 23902504679.0 / 1282400064.0;
    }

    CAL_DEF_OPS_CONSTEXPR(OldHinduLuniSolarDate)
    [[nodiscard]] CAL_CONSTEXPR_FN bool operator<(const OldHinduLuniSolarDate& o) const noexcept {
      return chain_lt(year, o.year, month, o.month, day, o.day, static_cast<int>(isLeapMonth), static_cast<int>(o.isLeapMonth));
    }
    
    [[nodiscard]] static CAL_CONSTEXPR_FN OldHinduLuniSolarDate from_date(const Date& date) noexcept {
      const auto sun          = static_cast<double>(OldHinduSolarDate::day_count(date)) + 6.0 / 24.0;
      const auto newMoon      = sun - math::mod(sun, OldHinduLuniSolarDate::aryaLunarMonth);
      const auto newMoonMonth = math::mod(newMoon, OldHinduSolarDate::aryaSolarMonth);
      const bool leap         = OldHinduSolarDate::aryaSolarMonth - OldHinduLuniSolarDate::aryaLunarMonth >= newMoonMonth && newMoonMonth > 0;
      const auto hMonth       = math::mod(math::ceil<double, int64_t>(newMoon / OldHinduSolarDate::aryaSolarMonth), 12) + 1;
      const auto hDay         = static_cast<int16_t>(math::mod(math::floor(sun / OldHinduLuniSolarDate::aryaLunarDay), 30) + 1);
      const auto hYear        = math::ceil<double, int64_t>((newMoon + OldHinduSolarDate::aryaSolarMonth) / OldHinduSolarDate::aryaSolarYear) - 1;
      return OldHinduLuniSolarDate{hYear, static_cast<OldHinduLuniSolarDate::MONTH>(hMonth), leap, hDay};
    }
    
    [[nodiscard]] CAL_CONSTEXPR_FN Date to_date() const noexcept {
      const auto mina         = static_cast<double>(12 * year - 1) * OldHinduSolarDate::aryaSolarMonth;
      const auto lunarNewYear = OldHinduLuniSolarDate::aryaLunarMonth * (math::floor(mina / OldHinduLuniSolarDate::aryaLunarMonth) + 1);
      const auto adjustment =
          !isLeapMonth
                  && math::ceil((lunarNewYear - mina) / (OldHinduSolarDate::aryaSolarMonth - OldHinduLuniSolarDate::aryaLunarMonth))
                         <= static_cast<double>(month)
              ? static_cast<int64_t>(month)
              : static_cast<int64_t>(month) - 1;
      return Date{math::ceil<double, int64_t>(OldHinduSolarDate::epoch + lunarNewYear + OldHinduLuniSolarDate::aryaLunarMonth * adjustment
                                              + (day - 1) * OldHinduLuniSolarDate::aryaLunarDay - 6.0 / 24.0)};
    }
  };
}
