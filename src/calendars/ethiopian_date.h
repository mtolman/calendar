#pragma once

#include "coptic_date.h"

namespace calendars {
  
  /**
   * Represents a date on the Ethiopian calendars
   */
  struct EthiopianDate {
    static constexpr int64_t epoch = 2796;

    int64_t year = 0;

    /**
     * Months for the Ethiopian calendars
     * Based on transliterations of the Amharic names
     */
    enum class MONTH : int16_t
    {
      MASKARAM = 1,
      TEQEMT   = 2,
      HEDAR    = 3,
      TAKHSAS  = 4,
      TER      = 5,
      YAKATIT  = 6,
      MAGABIT  = 7,
      MIYAZYA  = 8,
      GENBOT   = 9,
      SANE     = 10,
      HAMLE    = 11,
      NAHASE   = 12,
      PAGUEMEN = 13
    };

    MONTH month = MONTH::MASKARAM;

    int16_t day = 1;

    CAL_CONSTEXPR_CLASS_FN EthiopianDate() noexcept = default;
    CAL_CONSTEXPR_CLASS_FN EthiopianDate(int64_t year, MONTH month, int16_t day) noexcept : year(year), month(month), day(day) {}
    CAL_CONSTEXPR_CLASS_FN EthiopianDate(int64_t year, int16_t month, int16_t day) noexcept
        : EthiopianDate(year, static_cast<MONTH>(math::abs(math::amod(month, 13))), math::abs(day)) {}

    [[nodiscard]] CAL_CONSTEXPR_FN bool operator<(const EthiopianDate& o) const noexcept {
      return chain_lt(year, o.year, month, o.month, day, o.day);
    }

    CAL_DEF_OPS_CONSTEXPR(EthiopianDate)

    [[nodiscard]] static CAL_CONSTEXPR_FN bool is_leap_year(int64_t year) noexcept { return math::mod(year, 4) == 3; }
    
    [[nodiscard]] static CAL_CONSTEXPR_FN EthiopianDate from_date(const Date& rdDate) noexcept {
      const auto coptic = rdDate.add_days(CopticDate::epoch - EthiopianDate::epoch).to<CopticDate>();
      return EthiopianDate{coptic.year, static_cast<EthiopianDate::MONTH>(coptic.month), coptic.day};
    }
    
    [[nodiscard]] CAL_CONSTEXPR_FN Date to_date() const noexcept {
      const auto coptic = CopticDate{year, static_cast<CopticDate::MONTH>(month), day};
      return Date::from(coptic).add_days(EthiopianDate::epoch - CopticDate::epoch);
    }
  };
}