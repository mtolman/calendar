#pragma once

#include "unix_timestamp.h"

namespace calendars {

  /**
   * A Unix timestamp which represents the number of seconds which have elapsed since January 1,
   * 1970 (Gregorian) in UTC. Seconds are represented by a double and will track the fractional
   * pieces of a second (if there is enough precision).
   */
  struct MicroUnixTimestamp {
    /**
     * Number of seconds since January 1, 1970 (Gregorian) in UTC
     */
    double seconds;

    CAL_CONSTEXPR_CLASS_FN MicroUnixTimestamp() noexcept = default;
    CAL_CONSTEXPR_CLASS_FN MicroUnixTimestamp(double seconds) noexcept : seconds(seconds) {}

    [[nodiscard]] CAL_CONSTEXPR_FN bool operator<(const MicroUnixTimestamp& o) const noexcept { return seconds < o.seconds; }

    CAL_DEF_OPS_CONSTEXPR(MicroUnixTimestamp)

    [[nodiscard]] static CAL_CONSTEXPR_FN MicroUnixTimestamp from_moment(const Moment& moment) noexcept {
      return MicroUnixTimestamp{24 * 60 * 60 * (moment.datetime - static_cast<double>(UnixTimestamp::epoch))};
    }

    [[nodiscard]] CAL_CONSTEXPR_FN Moment to_moment() const noexcept {
      return Moment{UnixTimestamp::epoch + seconds / (24 * 60 * 60.0)};
    }
  };
}
