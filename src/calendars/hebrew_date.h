#pragma once

#include "common.h"

namespace calendars {
  /**
   * Standard Hebrew calendars
   * Note that this (and the associated holidays) may differ from the observed Hebrew calendars by a few days
   *  due to adjustments to keep the calendars in sync with astronomical events.
   */
  struct HebrewDate {
    static constexpr int64_t epoch = -1373427;

    enum class MONTH : int16_t
    {
      NISAN = 1,
      IYYAR,
      SIVAN,
      TAMMUZ,
      AV,
      ELUL,
      TISHRI,
      MARHESHVAN,
      KISLEV,
      TEVET,
      SHEVAT,
      ADAR,
      ADAR_II
    };

    CAL_CONSTEXPR_CLASS_FN HebrewDate() noexcept = default;
    CAL_CONSTEXPR_CLASS_FN HebrewDate(int64_t year, MONTH month, int16_t day) noexcept : year(year), month(month), day(day) {}
    CAL_CONSTEXPR_CLASS_FN HebrewDate(int64_t year, int16_t month, int16_t day) noexcept
        : HebrewDate(year, static_cast<MONTH>(math::abs(math::amod(month, 13))), math::abs(day)) {}

    int64_t year;
    MONTH   month;
    int16_t day;

    CAL_DEF_OPS_CONSTEXPR(HebrewDate)

    [[nodiscard]] CAL_CONSTEXPR_FN bool operator<(const HebrewDate& o) const noexcept {
      return chain_lt(year, o.year, month, o.month, day, o.day);
    }

    [[nodiscard]] static CAL_CONSTEXPR_FN bool is_leap_year(int64_t year) noexcept { return math::mod(7 * year + 1, 19) < 7; }

    [[nodiscard]] static CAL_CONSTEXPR_FN MONTH last_month_of_hebrew_year(int64_t year) noexcept { return is_leap_year(year) ? MONTH::ADAR_II : MONTH::ADAR; }

    [[nodiscard]] static CAL_CONSTEXPR_FN bool sabbatical_year(int64_t year) noexcept { return math::mod(year, 7) == 0; }

    [[nodiscard]] static CAL_CONSTEXPR_FN double molad(int64_t year, MONTH month) noexcept {
      const auto y = month < MONTH::TISHRI ? year + 1 : year;
      const auto monthsElapsed =
          static_cast<double>(month) - static_cast<double>(MONTH::TISHRI) + math::floor((235.0 * static_cast<double>(y) - 234.0) / 19.0);
      return static_cast<double>(epoch) - (876.0 / 25920.0) + monthsElapsed * (29 + 12.0 / 24.0 + 793.0 / 25920.0);
    }

    [[nodiscard]] static CAL_CONSTEXPR_FN int64_t elapsed_days(int64_t year) noexcept {
      using D                  = double;
      const auto monthsElapsed = math::floor<D, int64_t>(static_cast<D>(235 * year - 234) / 19.0);
      const auto partsElapsed  = 204 + 793 * math::mod(monthsElapsed, 1080);
      const auto hoursElapsed  = 11 + 12 * monthsElapsed + 793 * math::floor<D, int64_t>(static_cast<D>(monthsElapsed) / 1080.0)
                                + math::floor<D, int64_t>(static_cast<D>(partsElapsed) / 1080);
      const auto days = 29 * monthsElapsed + math::floor<D, int64_t>(static_cast<D>(hoursElapsed) / 24.0);
      return math::mod(3 * (days + 1), 7) < 3 ? days + 1 : days;
    }

    [[nodiscard]] static CAL_CONSTEXPR_FN int64_t year_length_correction(int64_t year) noexcept {
      const auto ny0 = elapsed_days(year - 1);
      const auto ny1 = elapsed_days(year);
      const auto ny2 = elapsed_days(year + 1);
      if (ny2 - ny1 == 356) {
        return 2;
      }
      else if (ny1 - ny0 == 382) {
        return 1;
      }
      else {
        return 0;
      }
    }

    [[nodiscard]] static CAL_CONSTEXPR_FN int64_t new_year(int64_t year) noexcept { return epoch + elapsed_days(year) + year_length_correction(year); }

    [[nodiscard]] static CAL_CONSTEXPR_FN int16_t last_day_of_month(int64_t year, MONTH month) noexcept {
      constexpr int16_t shortDays = 29;
      constexpr int16_t longDays  = 30;
      const auto        fixedShortMonth =
          month == MONTH::IYYAR || month == MONTH::TAMMUZ || month == MONTH::ELUL || month == MONTH::TEVET || month == MONTH::ADAR_II;
      if (fixedShortMonth) {
        return shortDays;
      }

      const auto shortAdar = month == MONTH::ADAR && !is_leap_year(year);
      if (shortAdar) {
        return shortDays;
      }

      const auto isLongMarheshvan = month == MONTH::MARHESHVAN && !long_marheshvan(year);
      if (isLongMarheshvan) {
        return shortDays;
      }

      const auto isShortKislev = month == MONTH::KISLEV && short_kislev(year);
      if (isShortKislev) {
        return shortDays;
      }

      return longDays;
    }

    [[nodiscard]] static CAL_CONSTEXPR_FN bool long_marheshvan(int64_t year) noexcept {
      const auto days = days_in_year(year);
      return days == 355 || days == 385;
    }

    [[nodiscard]] static CAL_CONSTEXPR_FN bool short_kislev(int64_t year) noexcept {
      const auto days = days_in_year(year);
      return days == 353 || days == 383;
    }

    [[nodiscard]] static CAL_CONSTEXPR_FN int64_t days_in_year(int64_t year) noexcept { return new_year(year + 1) - new_year(year); }
    
    [[nodiscard]] static CAL_CONSTEXPR_FN HebrewDate from_date(const Date& date) noexcept {
      const auto approx     = math::floor<double, int64_t>((98496 * static_cast<double>(date.day - HebrewDate::epoch)) / 35975351.0) + 1;
      int64_t    hebrewYear = approx - 1;
      
      for (int adj = 0; adj < 2 && HebrewDate::new_year(approx + adj) <= date.day; ++adj) {
        hebrewYear = approx + adj;
      }

      const auto start =
          date < Date::from(HebrewDate{hebrewYear, HebrewDate::MONTH::NISAN, 1}) ? HebrewDate::MONTH::TISHRI : HebrewDate::MONTH::NISAN;
      auto hebrewMonth = start;

      for (int16_t adj = 0;
           adj < 14 && date > Date::from(HebrewDate{hebrewYear, hebrewMonth, HebrewDate::last_day_of_month(hebrewYear, hebrewMonth)});
           adj += 1) {
        hebrewMonth = static_cast<HebrewDate::MONTH>(static_cast<int16_t>(start) + adj);
      }

      const auto hebrewDay = date.day_difference(Date::from(HebrewDate{hebrewYear, hebrewMonth, 1})) + 1;

      return HebrewDate{hebrewYear, hebrewMonth, static_cast<int16_t>(hebrewDay)};
    }
    
    [[nodiscard]] CAL_CONSTEXPR_FN Date to_date() const noexcept {
      int64_t calcMonth = 0;
      if (month < HebrewDate::MONTH::TISHRI) {
        auto lastMonth = HebrewDate::last_month_of_hebrew_year(year);
        for (auto m = static_cast<int16_t>(HebrewDate::MONTH::TISHRI); m <= static_cast<int16_t>(lastMonth) && m > 0; ++m) {
          calcMonth += HebrewDate::last_day_of_month(year, static_cast<HebrewDate::MONTH>(m));
        }
        for (auto m = static_cast<int16_t>(HebrewDate::MONTH::NISAN); m < static_cast<int16_t>(month) && m > 0; ++m) {
          calcMonth += HebrewDate::last_day_of_month(year, static_cast<HebrewDate::MONTH>(m));
        }
      }
      else {
        for (auto m = static_cast<int16_t>(HebrewDate::MONTH::TISHRI); m < static_cast<int16_t>(month) && m > 0; ++m) {
          calcMonth += HebrewDate::last_day_of_month(year, static_cast<HebrewDate::MONTH>(m));
        }
      }
      return Date{HebrewDate::new_year(year) + day - 1 + calcMonth};
    }
  };
}
