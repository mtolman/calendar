#pragma once

#include <optional>
#include "tzolkin.h"
#include "haab.h"

namespace calendars::mayan {
  [[nodiscard]] CAL_CONSTEXPR_FN Date haab_on_or_before(const Date& date, const Haab& haab) noexcept {
    return Date{math::mod_range(haab.ordinal() + Haab::epoch, date.day, date.day - 365)};
  }

  [[nodiscard]] CAL_CONSTEXPR_FN Date tzolkin_on_or_before(const Date& date, const Tzolkin& tzolkin) noexcept {
    return Date{math::mod_range(tzolkin.ordinal() + Tzolkin::epoch, date.day, date.day - 260)};
  }

  [[nodiscard]] CAL_CONSTEXPR_FN std::optional<Tzolkin::NAME> mayan_year_bearer(const Date& date) {
    if (date.to<Haab>().month == static_cast<Haab::MONTH>(19)) {
      return std::nullopt;
    }
    return haab_on_or_before(date, Haab{static_cast<Haab::MONTH>(1), 0}).to<Tzolkin>().name;
  }
  
  [[nodiscard]] CAL_CONSTEXPR_FN std::optional<Date> mayan_calendar_round_on_or_before(const Date& date, const Haab& haab, const Tzolkin& tzolkin) {
    const auto haabCount = haab.ordinal() + Haab::epoch;
    const auto tzolkinCount = tzolkin.ordinal() + Tzolkin::epoch;
    const auto diff = tzolkinCount - haabCount;
    if (math::mod(diff, 5) == 0) {
      return Date{math::mod_range(haabCount + 365 * diff, date.day, date.day - 18980)};
    }
    else {
      return std::nullopt;
    }
  }
}
