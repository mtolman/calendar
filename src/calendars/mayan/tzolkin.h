#pragma once

#include "long_date.h"

namespace calendars::mayan {
  /**
   * Represents a Mayan Tzolkin cycle wich tracks the number and name for the cycle.
   * Since a Mayan Tzolkin does not track which cycle it's in, we can only convert
   *  to the Mayan Tzolkin but not from a Tzolkin.
   */
  struct Tzolkin {
    enum class NAME
    {
      IMIX = 1,
      IK,
      AKBAL,
      KAN,
      CHICCHAN,
      CIMI,
      MANIK,
      LAMAT,
      MULUC,
      OC,
      CHUEN,
      EB,
      BEN,
      IX,
      MEN,
      CIB,
      CABAN,
      ETZNAB,
      CAUAC,
      AHAU,
    };

    int number; // 1-13
    NAME name; // 1-20

    [[nodiscard]] CAL_CONSTEXPR_FN int64_t ordinal() const {
      return math::mod(number - 1 + 39 * (number - static_cast<int>(name)), 260);
    }

    static constexpr int64_t epoch = LongDate::epoch - 159;

    CAL_DEF_OPS_CONSTEXPR(Tzolkin)

    [[nodiscard]] CAL_CONSTEXPR_FN bool operator<(const Tzolkin& o) const noexcept {
      if (number < o.number) return true;
      if (number != o.number) return false;
      return name < o.name;
    }

    [[nodiscard]] static CAL_CONSTEXPR_FN Tzolkin from_date(const Date& date) noexcept {
      const auto count = date.day - Tzolkin::epoch + 1;
      const auto number = math::amod(count, 13);
      const auto name = static_cast<Tzolkin::NAME>(math::amod(count, 20));
      return Tzolkin{static_cast<int>(number), name};
    }
  };
}
