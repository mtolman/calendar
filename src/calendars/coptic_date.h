#pragma once
#include "common.h"

namespace calendars {
  /**
   * Represents a date on the Coptic calendars
   */
  struct CopticDate {
    static constexpr int64_t epoch = 103605;

    int64_t year = 0;

    enum class MONTH : int16_t
    {
      THOOUT     = 1,
      PAOPE      = 2,
      ATHOR      = 3,
      KOIAK      = 4,
      TOBE       = 5,
      MESHIRE    = 6,
      PAREMOTEP  = 7,
      PARMOUTE   = 8,
      PASHONS    = 9,
      PAONE      = 10,
      EPEP       = 11,
      MESORE     = 12,
      EPAGOMENAE = 13
    };
    MONTH month = MONTH::THOOUT;
    int16_t day = 1;

    CAL_CONSTEXPR_CLASS_FN CopticDate() noexcept = default;
    CAL_CONSTEXPR_CLASS_FN CopticDate(int64_t year, MONTH month, int16_t day) noexcept : year(year), month(month), day(day) {}
    CAL_CONSTEXPR_CLASS_FN CopticDate(int64_t year, int16_t month, int16_t day) noexcept
        : CopticDate(year, static_cast<MONTH>(math::abs(math::amod(month, 13))), math::abs(day)) {}

    [[nodiscard]] CAL_CONSTEXPR_FN bool operator<(const CopticDate& o) const noexcept {
      return chain_lt(year, o.year, month, o.month, day, o.day);
    }

    CAL_DEF_OPS_CONSTEXPR(CopticDate)

    [[nodiscard]] static CAL_CONSTEXPR_FN bool is_leap_year(int64_t year) noexcept { return math::mod(year, 4) == 3; }
    
    [[nodiscard]] static CAL_CONSTEXPR_FN CopticDate from_date(const Date& rdDate) noexcept {
      const auto copticYear  = math::floor<double, int64_t>(static_cast<double>(4 * (rdDate.day - CopticDate::epoch) + 1463) / 1461.0);
      const auto copticMonth = static_cast<CopticDate::MONTH>(
          math::floor<double, int16_t>(static_cast<double>(rdDate.day_difference(Date::from(CopticDate{copticYear, CopticDate::MONTH::THOOUT, 1}))) / 30.0)
          + 1);
      const auto copticDay = static_cast<int16_t>(rdDate.add_days(1).day_difference(Date::from(CopticDate{copticYear, copticMonth, 1})));
      return CopticDate{copticYear, copticMonth, copticDay};
    }
    
    [[nodiscard]] Date CAL_CONSTEXPR_FN to_date() const noexcept {
      return Date{CopticDate::epoch - 1 + 365 * (year - 1) + math::floor<double, int64_t>(year / 4.0) + 30 * (static_cast<int64_t>(month) - 1) + day};
    }
  };
}