#pragma once

#include "radix_date.h"

namespace calendars {
  /**
   * Represents time based on a clock with an associated day count.
   */
  struct Clock {
    using RDate = RadixDate<0, 3, 24, 60, 60>;
    RDate date;

    /**
     * Creates a RadixDate from an array of values
     * @param values
     */
    CAL_CONSTEXPR_CLASS_FN explicit Clock(const std::array<double, RDate::Radix::ARRAY_NUM_SIZE>& values) noexcept : date(values) {}
    CAL_CONSTEXPR_CLASS_FN explicit Clock(RDate date) noexcept : date(std::move(date)) {}
    Clock(const Clock& values) = default;
    Clock(Clock&& o) noexcept  = default;
    Clock& operator=(const Clock& other) = default;
    Clock& operator=(Clock&& other) noexcept = default;
    ~Clock()                                 = default;

    /**
     * Creates a Clock from an initializer list
     * @param values
     */
    CAL_CONSTEXPR_CLASS_FN Clock(const std::initializer_list<double>& args) noexcept : date(args) {}

    /**
     * Returns a clock rounded to the nearest second
     * @return
     */
    [[nodiscard]] CAL_CONSTEXPR_FN Clock nearest_second() const noexcept {
      return Moment{static_cast<double>(math::round((time() * 24 * 60 * 60))) / (24.0 * 60 * 60)}.to<Clock>();
    }

    [[nodiscard]] CAL_CONSTEXPR_FN double              time() const noexcept { return Moment::from(*this).to_time(); }

    [[nodiscard]] CAL_CONSTEXPR_FN double day() const noexcept { return this->date.value[DAY]; }
    [[nodiscard]] CAL_CONSTEXPR_FN double hour() const noexcept { return this->date.value[HOUR]; }
    [[nodiscard]] CAL_CONSTEXPR_FN double minute() const noexcept { return this->date.value[MINUTE]; }
    [[nodiscard]] CAL_CONSTEXPR_FN double second() const noexcept { return this->date.value[SECOND]; }

    [[nodiscard]] CAL_CONSTEXPR_FN Clock withDay(double day) const noexcept {
      auto newValue = date;
      newValue[DAY] = day;
      return Clock{newValue};
    }
    [[nodiscard]] CAL_CONSTEXPR_FN Clock withHour(double hour) const noexcept {
      auto newValue = date;
      newValue[HOUR] = hour;
      return Clock{newValue};
    }
    [[nodiscard]] CAL_CONSTEXPR_FN Clock withMinute(double minute) const noexcept {
      auto newValue = date;
      newValue[MINUTE] = minute;
      return Clock{newValue};
    }
    [[nodiscard]] CAL_CONSTEXPR_FN Clock withSecond(double second) const noexcept {
      auto newValue = date;
      newValue[SECOND] = second;
      return Clock{newValue};
    }

    /**
     * Indexes for the clock components
     */
    enum CLOCK_COMPONENT
    {
      DAY,
      HOUR,
      MINUTE,
      SECOND
    };

    [[nodiscard]] static CAL_CONSTEXPR_FN Clock from_moment(const Moment& moment) noexcept {
      return Clock{moment.to<RDate>()};
    }

    [[nodiscard]] CAL_CONSTEXPR_FN Moment to_moment() const noexcept {
      return Moment::from(date);
    }

    [[nodiscard]] CAL_CONSTEXPR_FN bool operator<(const Clock& o) const noexcept {
      return date < o.date;
    }
  };
}
