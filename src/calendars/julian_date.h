#pragma once

#include "common.h"

namespace calendars {
  /**
   * Represents a date on the Gregorian calendars
   */
  struct JulianDate {
    static constexpr int64_t epoch = -1;

    /**
     * Year for the Julian Date
     * Positive years are A.D.
     * Negative years are B.C.
     */
    int64_t year = 1;

    enum class MONTH : int16_t
    {
      JANUARY   = 1,
      FEBRUARY  = 2,
      MARCH     = 3,
      APRIL     = 4,
      MAY       = 5,
      JUNE      = 6,
      JULY      = 7,
      AUGUST    = 8,
      SEPTEMBER = 9,
      OCTOBER   = 10,
      NOVEMBER  = 11,
      DECEMBER  = 12
    };

    /**
     * MONTH of the date
     * Between [1,12]
     */
    MONTH month = MONTH::JANUARY;

    /**
     * Day of the month for the date
     * Between [1,31]
     */
    int16_t day = 1;

    CAL_CONSTEXPR_CLASS_FN JulianDate() noexcept = default;
    CAL_CONSTEXPR_CLASS_FN JulianDate(int64_t year, MONTH month, int16_t day) noexcept : year(year == 0 ? -1 : year), month(month), day(day) {}
    CAL_CONSTEXPR_CLASS_FN JulianDate(int64_t year, int16_t month, int16_t day) noexcept
        : JulianDate(year, static_cast<MONTH>(math::abs(math::amod(month, 12))), math::abs(day)) {}

    [[nodiscard]] CAL_CONSTEXPR_FN bool operator<(const JulianDate& o) const noexcept {
      return chain_lt(year, o.year, month, o.month, day, o.day);
    }

    CAL_DEF_OPS_CONSTEXPR(JulianDate)

    [[nodiscard]] static CAL_CONSTEXPR_FN bool is_leap_year(int64_t year) { return math::mod(year, 4) == (year > 0 ? 0 : 3); }
    
    [[nodiscard]] static JulianDate CAL_CONSTEXPR_FN from_date(const Date& rdDate) noexcept {
      const auto approx     = math::floor<double, int64_t>(static_cast<double>(4 * rdDate.sub_days(JulianDate::epoch).day + 1464) / 1461.0);
      const auto julianYear = approx <= 0 ? approx - 1 : approx;
      const auto priorDays  = rdDate.day_difference(Date::from(JulianDate{julianYear, JulianDate::MONTH::JANUARY, 1}));
      const auto correction = rdDate < Date::from(JulianDate{julianYear, JulianDate::MONTH::MARCH, 1}) ? 0 : JulianDate::is_leap_year(julianYear) ? 1 : 2;
      const auto julianMonth = math::floor<double, int16_t>(static_cast<double>(12 * (priorDays + correction) + 373) / 367.0);
      const auto julianDay   = static_cast<int16_t>(rdDate.day_difference(Date::from(JulianDate{julianYear, julianMonth, 1})) + 1);
      return JulianDate{julianYear, julianMonth, julianDay};
    }
    
    [[nodiscard]] Date CAL_CONSTEXPR_FN to_date() const noexcept {
      const auto y = year < 0 ? year + 1 : year;
      return Date{JulianDate::epoch - 1 + 365 * (y - 1) + math::floor<double, int64_t>(static_cast<double>(y - 1) / 4.0)
                    + +math::floor<double, int64_t>((367.0 * static_cast<double>(month) - 362.0) / 12.0)
                    + (static_cast<int16_t>(month) <= 2 ? 0
                       : JulianDate::is_leap_year(year) ? -1
                                                          : -2)
                    + day};
    }
  };
}