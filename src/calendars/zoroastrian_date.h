#pragma once
#include "egyptian_date.h"

namespace calendars {
  /**
   * Represents an Armenian date which has the same structure as an Egyptian date, but it has a
   * different epoch.
   */
  struct ZoroastrianDate {
    static constexpr int64_t epoch = 230638;

    /**
     * Year of the date
     */
    int64_t year = 0;
    /**
     * MONTH of the year (1-13)
     */
    int16_t month = 1;
    /**
     * Day of the month: 1-30 (epagomenae 1-5)
     */
    int16_t day = 1;

    CAL_CONSTEXPR_CLASS_FN ZoroastrianDate() noexcept = default;
    CAL_CONSTEXPR_CLASS_FN ZoroastrianDate(int64_t year, int16_t month, int16_t day) noexcept
        : year(year), month(math::abs(math::amod(month, 13))), day(math::abs(day)) {}

    [[nodiscard]] CAL_CONSTEXPR_FN bool operator<(const ZoroastrianDate& o) const noexcept {
      return chain_lt(year, o.year, month, o.month, day, o.day);
    }

    CAL_DEF_OPS_CONSTEXPR(ZoroastrianDate)
    
    [[nodiscard]] static CAL_CONSTEXPR_FN ZoroastrianDate from_date(const Date& rdDate) {
      auto egyptian = rdDate.add_days(EgyptianDate::epoch - epoch).to<EgyptianDate>();
      return ZoroastrianDate{egyptian.year, egyptian.month, egyptian.day};
    }
    
    [[nodiscard]] CAL_CONSTEXPR_FN Date to_date() const noexcept {
      return Date{epoch + Date::from(EgyptianDate{year, month, day}).day - EgyptianDate::epoch};
    }
  };
}