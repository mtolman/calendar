#include "gregorian_date.h"


std::vector<calendars::Date> calendars::GregorianDate::year_vector(int64_t year) noexcept {
  std::vector<Date> res{};
  const auto          numDays = GregorianDate::is_leap_year(year) ? 366 : 365;
  res.reserve(numDays);
  const auto start = year_start(year);
  for (auto i = 0; i < numDays; ++i) {
    res.emplace_back(start.add_days(i));
  }
  return res;
}
