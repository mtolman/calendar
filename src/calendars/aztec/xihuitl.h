#pragma once

#include "common.h"

namespace calendars::aztec {

  /**
   * Represents an Aztec Xihuitl date.
   * Correlation is based on the fall of Mexico City to Hernán Cortés on August 13, 1521 (Julian)
   */
  struct Xihuitl {
    static constexpr int64_t correlation = details::correlation - 201;

    enum class MONTH : int16_t {
      IZCALI = 1,
      ATLCAHUALO,
      TLACAXIPEHUALIZTLI,
      TOZOZTONTLI,
      HUEI_TOZOZTLI,
      TOXCATL,
      ETZALCUALIZTLI,
      TECUILHUITONTLI,
      HUEI_TECUILHUITL,
      TLAXOCHIMACO,
      XOCOTLUETZI,
      OCHPANIZTLI,
      TEOTLECO,
      TEPEIHUITL,
      QUECHOLLI,
      PANQUETZALIZTLI,
      ATEMOZTLI,
      TITITL,
      NEMONTEMI
    };

    MONTH month;
    int16_t day;

    [[nodiscard]] CAL_CONSTEXPR_FN int ordinal() const {
      return (static_cast<int>(month) - 1) * 20 + static_cast<int>(day) - 1;
    }

    CAL_DEF_OPS_CONSTEXPR(Xihuitl)

    [[nodiscard]] CAL_CONSTEXPR_FN bool operator<(const Xihuitl& o) const noexcept {
      if (month < o.month) return true;
      if (month != o.month) return false;
      return day < o.day;
    }

    [[nodiscard]] static CAL_CONSTEXPR_FN Xihuitl from_date(const Date& date) noexcept {
      const auto count = math::mod(date.day - Xihuitl::correlation, 365);
      const auto aztecDay = static_cast<int16_t>(math::mod(count, 20) + 1);
      const auto aztecMonth = static_cast<Xihuitl::MONTH>(math::floor<double, int>(static_cast<double>(count) / 20.0) + 1);
      return Xihuitl{aztecMonth, aztecDay};
    }
  };
}
