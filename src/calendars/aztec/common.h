#pragma once

#include "../common.h"

namespace calendars::aztec::details {
  // Correlation is based on the fall of Mexico City to Hernán Cortés on August 13, 1521 (Julian)
  static constexpr auto correlation = 555403;
}