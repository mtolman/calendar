#pragma once

#include "common.h"

namespace calendars::aztec {
  /**
   * Represents an Aztec Tonalpohualli date.
   */
  struct Tonalpohualli {
    static constexpr int64_t correlation = 555299;
    
    enum class NAME : int16_t {
      CIPACTLI = 1,
      EHECATL,
      CALLI,
      CUETZPALLIN,
      COATL,
      MIQUIZTLI,
      MAZATL,
      TOCHTLI,
      ATL,
      ITZCUINTLI,
      OZOMATLI,
      MALINALLI,
      ACATL,
      OCELOTL,
      QUAUHTLI,
      COZCAQUAUHTLI,
      OLLIN,
      TECPATL,
      QUIAHUITL,
      XOCHITL
    };

    int16_t number;
    NAME name;

    [[nodiscard]] CAL_CONSTEXPR_FN int ordinal() const noexcept {
      return math::mod(static_cast<int>(number) - 1 + 39 * (static_cast<int>(number) - static_cast<int>(name)), 260);
    }

    CAL_DEF_OPS_CONSTEXPR(Tonalpohualli)

    [[nodiscard]] CAL_CONSTEXPR_FN bool operator<(const Tonalpohualli& o) const noexcept {
      if (number < o.number) return true;
      if (number != o.number) return false;
      return name < o.name;
    }
    
    [[nodiscard]] static CAL_CONSTEXPR_FN Tonalpohualli from_date(const Date& date) noexcept {
      const auto count = date.day - Tonalpohualli::correlation + 1;
      const auto number = math::amod(count, 13);
      const auto name = math::amod(count, 20);
      return Tonalpohualli{static_cast<int16_t>(number), static_cast<Tonalpohualli::NAME>(name)};
    }
  };
}