# Calendar Library

This is a calendars library which supports the following calendars and date tracking methods:

- Armenian Calendar
- Coptic Calendar
- Egyptian Calendar
- Ethiopian Calendar
- Hebrew Calendar (Standard)
- Icelandic Calendar
- Islamic Calendar (Arithmetic)
- Gregorian Calendar
- Julian Calendar
- Julian Day
- Micro Unix Timestamp
- Modified Julian Day
- Unix Timestamp
- Zoroastrian Calendar

## Observed Calendars

Some calendars, such as the Islamic and Hebrew calendars, have real-world authorities who adjust the calendars based on observations of astronomical events.
These adjusted calendars are the versions of the calendars which are generally followed by those who use that calendaring system.

Because the observed calendars are based on adjustments made by various real-world leaders, they cannot be predicted.
However, these calendars may be approximated, often to within a day or two. For some use cases the approximation may be all that's required.
If more accuracy is needed, then one must continuously procure updated data from the authorities of the observed calendars and incorporate that data into the calendrical calculations.

This repository makes no attempt to pull in or incorporate real-world data into the calendrical calculations.

Implemented calendaring systems with associated observed calendars are:
- Hebrew Calendar
- Islamic Calendar

## Compile Definitions

### CAL_USE_CONSTEXPR
If provided, it will determine if all conversions will be constexpr or not.

By default, C++20 is constexpr while C++17 will only be constexpr if there is a compiler builtin for __builtin_is_constant_evaluated.

The reason for this is C++20 can avoid runtime penalties in mathematical operations while C++17 cannot.
Some math functions, like floor, are software-implemented for constexpr to work.
However, software implementations of these math functions are generally slower than the hardware implementations.
In C++20, constexpr functions can know if they are running at compile time or at runtime.
If a constexpr function is running at runtime, then it will default to the standard implementation of the math function, which often gets
compiled into native machine instructions by the compiler.
However, when the function runs at compile time, it will fall back to the software constexpr definition.

C++17 constexpr functions cannot determine if they are running at compile time or runtime unless there is a compiler built-in.
Consequently, there may be cases where they cannot use the faster standard implementations at runtime and must use the slower software implementation in order to be constexpr.
When there would be a runtime performance, C++17 will forgo constexpr. GCC 9+ and Clang 9+ provide the __builtin_is_constant_evaluated built-in. 

If you are using an older C++17 compiler which does not support __builtin_is_constant_evaluated, then you can still force constexpr with CAL_USE_CONSTEXPR.
Doing so can also be useful if you are using MSVC or older versions of GCC or Clang, and you wish for C++17 constexpr support.
Be aware that by overriding CAL_USE_CONSTEXPR, then on older compilers you may incur a runtime cost due to software implementations of core math functions.

The conversion code is based on the mathematics outlined in the book "Calendrical Calculations: The Ultimate Edition " by Edward M. Reingold and Nachum Dershowltz (**ISBN-13**: 978-1107683167; **ISBN-10** 1107683165).

## PHP Extension Compilation

Make sure you install PHP and PHP-CPP (both are needed for build to work).
Turn on the CMAKE option BUILD_PHP.
