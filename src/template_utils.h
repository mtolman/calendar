#pragma once

#include <type_traits>
#include <variant>

namespace templ::utils {
  template<int... A>
  struct containsZero;

  template<>
  struct containsZero<> {
    static constexpr auto value = std::false_type::value;
  };

  template<int B, int... A>
  struct containsZero<B, A...> {
    static constexpr auto value = (B == 0) || containsZero<A...>::value;
  };

  template<int... A>
  struct containsNegative;

  template<>
  struct containsNegative<> {
    static constexpr auto value = std::false_type::value;
  };

  template<int B, int... A>
  struct containsNegative<B, A...> {
    static constexpr auto value = (B < 0) || containsNegative<A...>::value;
  };

  template<typename Target, typename... Types>
  struct VariantContainsType;

  template<typename Target, typename... Types>
  struct VariantContainsType<Target, std::variant<Target, Types...>> {
    static constexpr bool value = true;
  };

  template<typename Target, typename First, typename... Types>
  struct VariantContainsType<Target, std::variant<First, Types...>> {
    static constexpr bool value = VariantContainsType<Target, std::variant<Types...>>::value;
  };

  template<typename Target>
  struct VariantContainsType<Target, std::variant<Target>> {
    static constexpr bool value = true;
  };

  template<typename Target, typename First>
  struct VariantContainsType<Target, std::variant<First>> {
    static constexpr bool value = false;
  };

  namespace impl {
    template<class X, class Y, class Op>
    struct HasOperator {
      template<class O, class L, class R>
      static auto check(int /*unused*/) noexcept -> decltype(std::declval<O>()(std::declval<L>(), std::declval<R>()), void(), std::true_type());

      template<class O, class L, class R>
      static auto check(double /* unused */) noexcept -> std::false_type;

      using type                  = decltype(check<Op, X, Y>(0));
      static constexpr auto value = type::value;
    };
  }  // namespace impl

  template<class X, class Y, class Op>
  using HasOperator = typename impl::HasOperator<X, Y, Op>;

  template<class X, class Y = X>
  inline constexpr bool hasEquality = HasOperator<X, Y, std::equal_to<>>::value;
  template<class X, class Y = X>
  inline constexpr bool hasInequality = HasOperator<X, Y, std::not_equal_to<>>::value;
  template<class X, class Y = X>
  inline constexpr bool hasLessThan = HasOperator<X, Y, std::less<>>::value;
  template<class X, class Y = X>
  inline constexpr bool hasLessThanOrEqualsTo = HasOperator<X, Y, std::less_equal<>>::value;
  template<class X, class Y = X>
  inline constexpr bool hasGreaterThan = HasOperator<X, Y, std::greater<>>::value;
  template<class X, class Y = X>
  inline constexpr bool hasGreaterThanOrEqualsTo = HasOperator<X, Y, std::greater_equal<>>::value;


  #define DEFINE_HAS_SIGNATURE(traitsName, funcName, signature)               \
      template <typename U>                                                   \
      class traitsName                                                        \
      {                                                                       \
      private:                                                                \
          template<typename T, T> struct helper;                              \
          template<typename T>                                                \
          static std::uint8_t check(helper<signature, &funcName>*);           \
          template<typename T> static std::uint16_t check(...);               \
      public:                                                                 \
          static                                                              \
          constexpr bool value = sizeof(check<U>(0)) == sizeof(std::uint8_t); \
      }
}  // namespace templ::utils
